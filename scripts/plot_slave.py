#!/usr/bin/python

import matplotlib.pyplot as plt
import sys

filename = sys.argv[1]
print("Filename {}".format(filename))

f = open(filename,"r")
lines = f.readlines()

timestamps = []
values = []

for line in lines:
    contents = line.split(' ')
    timestamps.append(float(contents[int(sys.argv[2])]))
    values.append(float(contents[int(sys.argv[3])]))

plt.plot(timestamps,values)
plt.show()



