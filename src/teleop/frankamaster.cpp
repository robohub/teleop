#include <teleop/teleop_franka.hpp>
// franka
#include <franka/exception.h>
#include <franka/robot.h>
#include "examples_common.h"

namespace teleop {

FrankaMaster::FrankaMaster(std::string const & remote_addr, char channel, char gripper_channel, char joint_channel)
    : CartesianMaster(remote_addr,channel), GripperMaster(CartesianMaster::ep_, gripper_channel, joint_channel) {
    // initialize robot
    robot_.reset(new franka::Robot("franka1"));
    robot_->automaticErrorRecovery();
    robot_->setCollisionBehavior(
            {{50.0, 50.0, 50.0, 50.0, 20.0, 20.0, 20.0}}, {{50.0, 50.0, 50.0, 50.0, 20.0, 20.0, 20.0}},
            {{40.0, 40.0, 40.0, 40.0, 10.0, 10.0, 10.0}}, {{40.0, 40.0, 40.0, 40.0, 10.0, 10.0, 10.0}},
            {{20.0, 20.0, 20.0, 20.0, 20.0, 20.0}}, {{20.0, 20.0, 20.0, 20.0, 20.0, 20.0}},
            {{40.0, 40.0, 40.0, 10.0, 10.0, 10.0}}, {{40.0, 40.0, 40.0, 10.0, 10.0, 10.0}});
    robot_->setJointImpedance({{1500, 1500, 1500, 1250, 1250, 1250, 1250}});
    // load the kinematics and dynamics model
    robot_model_.reset(new franka::Model(robot_->loadModel()));
    //franka::RobotState initial_state = robot.readOnce();
    //
}

FrankaMaster::~FrankaMaster() {}

void FrankaMaster::robot_startup() {
        // TODO: recover robot if neccessary
        // First move the robot to a suitable joint configuration
        std::array<double, 7> q_goal = {{M_PI_2, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
        MotionGenerator motion_generator(0.5, q_goal);
        robot_->control(motion_generator);
        std::cout << "Finished moving to initial joint configuration." << std::endl;
}

void FrankaMaster::keyboard_callback(int c) {
    GripperMaster::keyboard_callback(c);
}


void FrankaMaster::robot_control() {
    std::array<double,6> cartesian_acceleration;
    cartesian_acceleration.fill(0.);
        try {
            Eigen::Matrix<double,6,1> cartesian_wrench_filtered;
            cartesian_wrench_filtered.setZero();
            Eigen::Matrix<double,6,1> last_velocity, cartesian_acceleration_filtered;
            bool last_velocity_initialized = false;
            cartesian_acceleration_filtered.setZero();
            unsigned int packets_received = 0;
            double average_timestamp_difference = 0.;
            user_stop_ = false; 
            // define callback for the torque control loop
            std::function<franka::Torques(const franka::RobotState&, franka::Duration)>
                impedance_control_callback = [&](const franka::RobotState& robot_state,
                        franka::Duration /*duration*/) -> franka::Torques {
                    // get state variables
                    std::array<double, 7> coriolis_array = robot_model_->coriolis(robot_state);
                    std::array<double, 42> jacobian_array =
                        robot_model_->zeroJacobian(franka::Frame::kEndEffector, robot_state);

                    // convert to Eigen
                    Eigen::Map<const Eigen::Matrix<double, 7, 1> > coriolis(coriolis_array.data());
                    Eigen::Map<const Eigen::Matrix<double, 6, 7> > jacobian(jacobian_array.data());
                    Eigen::Map<const Eigen::Matrix<double, 7, 1> > dq(robot_state.dq.data());

                    // compute control & report state to slave
                    Eigen::VectorXd tau_task(7), tau_d(7);
                    std::array<double,6> cartesian_velocity;
                    Eigen::Map<Eigen::Matrix<double,6,1> > eigen_cartesian_velocity(cartesian_velocity.data());
                    Eigen::Map<Eigen::Matrix<double,6,1> > eigen_cartesian_acceleration(cartesian_acceleration.data());
                    eigen_cartesian_velocity = jacobian * dq;
                    if(!last_velocity_initialized) {
                        last_velocity = eigen_cartesian_velocity;
                        last_velocity_initialized = true;
                    }
                    double gripper_dqd, workspace_scaling;
                    bool gripper_grasp;
                    {
                        std::lock_guard<std::mutex> lock(gripper_mutex_);
                        gripper_dqd = gripper_dqd_;
                        gripper_grasp = gripper_grasp_;
                    }
                    {
                        std::lock_guard<std::mutex> lock(keyboard_mutex_);
                        workspace_scaling = workspace_scaling_;
                    }
                    eigen_cartesian_velocity *= workspace_scaling;

                    Eigen::Matrix<double,6,1> cartesian_acceleration_raw = eigen_cartesian_velocity - last_velocity;
                    last_velocity = eigen_cartesian_velocity;
                    cartesian_acceleration_filtered = 0.05 * ( cartesian_acceleration_raw - cartesian_acceleration_filtered ) ;
                    eigen_cartesian_acceleration = workspace_scaling * cartesian_acceleration_filtered;
                    eigen_cartesian_acceleration.setZero();

                    gripper_dqd *= workspace_scaling;
                    
                    // apply workspace rotation
                    eigen_cartesian_velocity.head(3) = master_workspace_rotation_ * eigen_cartesian_velocity.head(3);
                    eigen_cartesian_velocity.tail(3) = master_workspace_rotation_ * eigen_cartesian_velocity.tail(3);
                    eigen_cartesian_acceleration.head(3) = master_workspace_rotation_ * eigen_cartesian_acceleration.head(3);
                    eigen_cartesian_acceleration.tail(3) = master_workspace_rotation_ * eigen_cartesian_acceleration.tail(3);
                    // do not forward velocities to slave if clutch is disconnected
                    if(clutch_disabled_) {
                        cartesian_velocity.fill(0.);
                        cartesian_acceleration.fill(0.);
                        gripper_dqd = 0.;
                    }
                    send_packet(pbwbc::teleoperation_messages::master_state::FREE,cartesian_velocity,cartesian_acceleration);
                    gripper_send_packet(pbwbc::teleoperation_messages::master_state::FREE,gripper_dqd, gripper_grasp);
                    gripper_send_packet_joint_msg(pbwbc::teleoperation_messages::master_state::FREE, gripper_dqd, gripper_grasp);
                    // TODO: send_joint_packet() .... for talos gripper
                    // TODO:
                    // send gripper status on separate channel
                    {
                        std::lock_guard<std::mutex> lock(packet_in_mutex_);
                        Eigen::Map<Eigen::Matrix<float,6,1> > eigen_cartesian_wrench(packet_in_.cartesian_wrench.data());
                        const double force_scaling_factor = 1.;
                        const double torque_scaling_factor = 0.;
                        const double wrench_filter_factor = 1.;
                        const double timestamp_difference =
                            std::chrono::duration<double>(std::chrono::system_clock::now() - packet_in_timestamp_).count();
                        // reject packets with larger jitter after initialization of average_timestamp_difference
                        // also: do not reflect forces if clutch is disconnected
                        if(packets_received < 100 || timestamp_difference < 1.3 * average_timestamp_difference || clutch_disabled_) {
                            cartesian_wrench_filtered.head(3) += 
                                wrench_filter_factor * (force_scaling_factor * 
                                        master_workspace_rotation_.transpose() * eigen_cartesian_wrench.head(3).cast<double>() - cartesian_wrench_filtered.head(3));
                            cartesian_wrench_filtered.tail(3) +=
                                wrench_filter_factor * (
                                        master_workspace_rotation_.transpose() * torque_scaling_factor * eigen_cartesian_wrench.tail(3).cast<double>() - cartesian_wrench_filtered.tail(3));
                        } else {
                            cartesian_wrench_filtered.head(3) += 
                                0.01 * ( - cartesian_wrench_filtered.head(3));
                            cartesian_wrench_filtered.tail(3) +=
                                0.01 * ( - cartesian_wrench_filtered.tail(3));
                        }
                        // saturate forces to protect user
                        // TODO: soft saturation of forces (self collision avoidance controller)
                        cartesian_wrench_filtered = cartesian_wrench_filtered.array().min(20.).max(-20.);
                        average_timestamp_difference += 0.01 * (timestamp_difference - average_timestamp_difference);
                        packets_received++;
                    }
                    if(CartesianMaster::counter_%10000==0) {
                        std::cout << "Average timestamp difference: " << average_timestamp_difference << std::endl;
                    }


                    // fake user input for debugging
                    //double exec_time =
                    //    std::chrono::duration<double>(std::chrono::system_clock::now() - free_state_start).count();
                    //if(3. < exec_time && exec_time < 20.) {
                    //    eigen_cartesian_wrench(1) += 3 * sin(exec_time * 2. * 3.14 * 0.5);
                    //}
                    // filter
                    tau_task << jacobian.transpose() * cartesian_wrench_filtered;
                    tau_d << tau_task + coriolis;

                    if(clutch_disabled_) {
                        tau_d.setZero();
                    }
                    franka::Torques tau_d_array{0.,0.,0.,0.,0.,0.,0.};
                    Eigen::VectorXd::Map(tau_d_array.tau_J.data(), 7) = tau_d;
                    // handle termination request
                    // - connection lost (connection_timeout_)
                    // - slave switched off (slave state)
                    // - local termination request (Ctrl+C)
                    bool is_packet_valid = packet_valid();
                    using pbwbc::teleoperation_messages::slave_state;
                    bool is_slave_stopped =
                        last_slave_state_==slave_state::RUNNING && packet_in_.state==slave_state::STANDBY;
                    if (!is_packet_valid || is_slave_stopped || !keep_running_ || user_stop_) {
                        std::cout << "Stopping: ";
                        if(!is_packet_valid)std::cout << "Connection timeout";
                        if(is_slave_stopped)std::cout << "Slave stopped";
                        if(!keep_running_)std::cout << "Local termination request";
                        if(user_stop_)std::cout << "User stop";
                        std::cout << std::endl;
                        return franka::MotionFinished(tau_d_array);
                    }
                    last_slave_state_ = packet_in_.state;
                    return tau_d_array;
                };

            robot_->control(impedance_control_callback);
            keyboard_thread_keep_running_ = false;
        } catch (const franka::Exception& e) {
            keyboard_thread_keep_running_ = false;
            std::cout << "Robot error: " << e.what() << std::endl;
            std::cout << "Press enter to recover" << std::endl;
            wait_for_key();
            robot_->automaticErrorRecovery();
        }
}

} // namespace

