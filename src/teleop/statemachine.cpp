#include <teleop/teleop.hpp>

namespace teleop {

StateMachine::StateMachine() {}
StateMachine::~StateMachine() {}

void StateMachine::add_state(std::shared_ptr<State> const & state) {
    states_.push_back(state);
}
void StateMachine::add_transition(
        std::shared_ptr<State> const & a,
        std::shared_ptr<State> const & b,
        check_function_t const & function) {
    transitions_.push_back(std::make_tuple(a,b,function));
}

void StateMachine::start() {
    current_state_ = states_.at(0);
    current_state_->start();
}

void StateMachine::run() {
    current_state_->run();
    // check transitions
    for(auto const & transition : transitions_) {
        if(std::get<0>(transition)==current_state_
                && std::get<2>(transition)()) {
            current_state_->stop();
            current_state_ = std::get<1>(transition);
            current_state_->start();
            break;
        }
    }
}

void StateMachine::stop() {
    current_state_->stop();
}

}

