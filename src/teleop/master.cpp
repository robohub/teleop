#include <teleop/teleop.hpp>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <arpa/inet.h>
#include <signal.h>
#include <termios.h>
#include <linux/socket.h>
#include <regex>
#include <iomanip>

namespace {
std::function<void(int)> signal_handler;
void c_signal_handler(int signal) {signal_handler(signal);}
}

namespace teleop {

CartesianMaster::CartesianMaster(std::string const & remote_addr,
        char channel)
    : ep_(pbwbc::TeleOpEndpoint::getInstance(remote_addr.size() ? 0 : 2323) ) {
    std::cout << "Master initializing ... " << std::endl;
    
    packet_valid_ = false;
    counter_ = 0;
    channel_ = channel;
    
    // Initialize connection from master side.
    // Send a first packet to the slave to initialize the address there
    if(remote_addr.size()) {
        std::cout << "Initiating connection to slave" << std::endl;
        std::smatch regex_results;
        if(!std::regex_match(remote_addr,regex_results,std::regex("\\[(.*)\\]:([0-9]+)$"))) {
            throw std::runtime_error("Could not parse ip address");
        }
        //inet_pton(AF_INET6, regex_results.str(1).c_str(), &(servaddr_.sin6_addr));
        ep_.connect(regex_results.str(1),regex_results.str(2));
        send_packet(
                pbwbc::teleoperation_messages::master_state::STANDBY,
                std::array<double,6>(),std::array<double,6>());
    }
    
    
    keep_running_ = true;
    signal_handler = [&](int signal){
        std::cout << "Ctrl+C received, shutting down" << std::endl;
        this->keep_running_ = false;
    };
    signal(SIGINT, c_signal_handler);

    connection_timeout_ = 0.4;
    workspace_scaling_ = 1.;

    master_workspace_rotation_ =
        Eigen::AngleAxisd(0.*M_PI, Eigen::Vector3d::UnitZ()).toRotationMatrix();

    ep_.register_channel_callback(channel_,
            [&](char channel, char *buffer, std::size_t buffer_size) {
                this->teleop_callback(channel,buffer,buffer_size);
            });

    std::cout << "done" << std::endl;

    log_fd_.open("/dev/shm/cartesian_master.log");
}

CartesianMaster::~CartesianMaster() {
    std::cout << "Shutting down master ... ";
    std::cout << "done" << std::endl;
}
    
void CartesianMaster::teleop_callback(char channel, char * buffer, char buffer_size) {
    if(channel_!=channel) {
        std::cout << "Packet on unexpected channel" << std::endl;
        return;
    }
    if(buffer_size!=sizeof(pbwbc::teleoperation_messages::cartesian_slave2master)) {
        throw std::runtime_error("Corrupted feedback packet");
    }
    std::lock_guard<std::mutex> lock(packet_in_mutex_);
    typedef pbwbc::teleoperation_messages::cartesian_slave2master msg_t;
    auto new_packet = *(msg_t*)&buffer[0];
    if(packet_valid_) {
        int delta = new_packet.counter - packet_in_.counter;
        if(delta>1) {
            std::cout << "\n Packet(s) lost: " << delta-1 << std::endl;
        }else if(delta<1) {
            std::cout << "\n Packets reordered, ignoring" << std::endl;
            return;
        }
    }
    packet_in_ = *(pbwbc::teleoperation_messages::cartesian_slave2master*)&buffer[0];
    packet_valid_ = true;
    packet_in_timestamp_ = std::chrono::system_clock::now();
    std::lock_guard<std::mutex> log_lock(log_fd_mutex_);
    log_fd_ << "I " << packet_in_.counter << " " <<  std::setprecision(15) << packet_in_.timestamp 
        << " " << std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count(); 
    std::array<float,6> cartesian_wrench = packet_in_.cartesian_wrench;
    for(auto const & v : cartesian_wrench) {
        log_fd_ << " " << std::setprecision(6) << v;
    }
    log_fd_ << std::endl;
}


bool CartesianMaster::packet_valid() {
    std::lock_guard<std::mutex> lock(packet_in_mutex_);
    double last_packet_received = 
        std::chrono::duration<double>(
           std::chrono::system_clock::now() - packet_in_timestamp_).count();
    return packet_valid_ && (last_packet_received < connection_timeout_);
}

void CartesianMaster::send_packet(
        pbwbc::teleoperation_messages::master_state const & state,
        std::array<double,6> const & cartesian_velocity,
        std::array<double,6> const & cartesian_acceleration) {
    typedef pbwbc::teleoperation_messages::cartesian_master2slave packet_master2slave;
    char output_buffer[1+sizeof(packet_master2slave)];
    output_buffer[0] = channel_; 
    packet_master2slave *packet_out = reinterpret_cast<packet_master2slave*>(&output_buffer[1]);
    packet_out->counter = counter_++;
    packet_out->timestamp = std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count();
    packet_out->state = state;
    for(std::size_t idx=0;idx<6;idx++) {
        packet_out->cartesian_velocity[idx] = cartesian_velocity[idx];
        packet_out->cartesian_acceleration[idx] = cartesian_acceleration[idx];
    }

    ep_.send_feedback(
            output_buffer,
            sizeof(output_buffer));
    std::lock_guard<std::mutex> log_lock(log_fd_mutex_);
    log_fd_ << "O " << packet_out->counter << " " <<  std::setprecision(15) << packet_out->timestamp;
    for(auto const & v : cartesian_velocity) {
        log_fd_ << " " << std::setprecision(6) << v;
    }
    log_fd_ << std::endl;
}

void CartesianMaster::wait_for_key() {
    while(keep_running_) {
        fd_set set;
        char buff[100];
        FD_ZERO(&set); /* clear the set */
        FD_SET(STDIN_FILENO, &set); /* add our file descriptor to the set */
        struct timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;
        int rv = select(STDIN_FILENO + 1, &set, NULL, NULL, &timeout);
        if(rv > 0) {
            int len = 100;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
            read( STDIN_FILENO, buff, len ); /* there was data to read */
#pragma GCC diagnostic pop
            break;
        }
    }
}

void CartesianMaster::execute() {
    while(keep_running_) {
        std::cout << "Hit enter to move robot back to start" << std::endl;
        wait_for_key();
        if(!keep_running_)break;
        // invalidate old packet / slave state
        {
            std::lock_guard<std::mutex> lock(packet_in_mutex_);
            packet_valid_ = false;
        }
        robot_startup();

        std::cout << "Waiting for slave to switch to STANDBY" << std::endl;
        while(keep_running_) {
            // TODO: check if last packet is actually recent
            if(packet_valid() && packet_in_.state==
                    pbwbc::teleoperation_messages::slave_state::STANDBY)break;
            send_packet(
                    pbwbc::teleoperation_messages::master_state::STANDBY,
                    std::array<double,6>(),std::array<double,6>());
            std::this_thread::sleep_for (std::chrono::milliseconds(10));
        }
        if(!keep_running_)break;
        std::cout << "Slave is now in STANDBY, switching master to FREE" << std::endl;
        //auto free_state_start = std::chrono::system_clock::now();
        last_slave_state_ = packet_in_.state;
        // start keyboard scan thread here to detect pressed space bar -> send zero velocities to
        // slave for indexing. Actually decay velocities so filtering can be limited to actual values
        clutch_disabled_ = false;
        keyboard_thread_keep_running_ = true;
        std::thread keyboard_thread([&](){
                struct termios savedState;
                struct termios newState;
                if (-1 == tcgetattr(STDIN_FILENO, &savedState)) {
                    throw std::runtime_error("failed to get termios settings");
                }
                newState = savedState;
                // termios: block for 100ms, then return if no key pressed
                // return immediately if key pressed
                newState.c_lflag &= ~(ECHO | ICANON);
                newState.c_cc[VMIN] = 0;
                newState.c_cc[VTIME] = 1;

                if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &newState)) {
                    throw std::runtime_error("failed to set termios settings");
                }
                while(keyboard_thread_keep_running_) {
                    int c = getchar();
                    if(c!=-1) { // empty stdin buffer now
                        char buf[100];
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
                        read( STDIN_FILENO, buf, 100 );
#pragma GCC diagnostic pop
                    }
                    // forward keys to specific master implementation
                    keyboard_callback(c);
                    // process generic input
                    if(c==-1) {
                        if(clutch_disabled_) { 
                            std::cout << "Clutch enabled" << std::endl;
                            clutch_disabled_ = false;
                        }
                        continue;
                    } else if (c=='e') {
                        std::lock_guard<std::mutex> lock(keyboard_mutex_);
                        user_stop_ = true; 
                    } else if (c=='c') {
                        {
                            std::lock_guard<std::mutex> lock(keyboard_mutex_);
                            workspace_scaling_ = std::min(1.,workspace_scaling_*1.5);
                        }
                        std::cout << "Workspace scaling: " << workspace_scaling_ << std::endl;
                    } else if (c=='v') {
                        {
                            std::lock_guard<std::mutex> lock(keyboard_mutex_);
                            workspace_scaling_ = std::max(0.,workspace_scaling_/1.5);
                        }
                        std::cout << "Workspace scaling: " << workspace_scaling_ << std::endl;
                    } else if (c==' ') {
                        //std::lock_guard<std::mutex> lock(keyboard_mutex_);
                        if(!clutch_disabled_) { 
                            clutch_disabled_ = true;
                            std::cout << "Clutch disabled" << std::endl;
                        }
                    }
                }
                if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &savedState)) {
                    throw std::runtime_error("failed to restore termios settings");
                }
            });
        robot_control();
        keyboard_thread.join();
        // report to master that slave is no longer running
        send_packet(
                pbwbc::teleoperation_messages::master_state::STANDBY,
                std::array<double,6>(), std::array<double,6>());
    }
}

void CartesianMaster::keyboard_callback(int ){}


GripperMaster::GripperMaster(pbwbc::TeleOpEndpoint & ep, char gripper_channel, char joint_channel)
    : ep_(ep), gripper_channel_(gripper_channel), joint_channel_(joint_channel), counter_(0) {
        std::cout << "Gripper channel " << static_cast<int>(gripper_channel_&0xFF) << std::endl;
        gripper_grasp_ = 0.;
        gripper_dqd_ = 0.;
    }

GripperMaster::~GripperMaster(){}

void GripperMaster::gripper_send_packet(
            pbwbc::teleoperation_messages::master_state const & state,
            double gripper_velocity,
            bool gripper_grasp) {
    char output_buffer[1+sizeof(pbwbc::teleoperation_messages::gripper_master2slave)];
    output_buffer[0] = gripper_channel_;
    pbwbc::teleoperation_messages::gripper_master2slave *packet_out = 
        reinterpret_cast<pbwbc::teleoperation_messages::gripper_master2slave*>(&output_buffer[1]);
    packet_out->counter = counter_++;
    packet_out->timestamp = std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count();
    packet_out->state = state;
    packet_out->gripper_velocity = gripper_velocity;
    packet_out->gripper_grasp = gripper_grasp;
    ep_.send_feedback(
            output_buffer,
            sizeof(output_buffer));
}

void GripperMaster::gripper_send_packet_joint_msg(
            pbwbc::teleoperation_messages::master_state const & state,
            double gripper_velocity,
            bool ) {
    char output_buffer[1+sizeof(pbwbc::teleoperation_messages::joint_master2slave)];
    output_buffer[0] = joint_channel_;
    pbwbc::teleoperation_messages::joint_master2slave *packet_out = 
        reinterpret_cast<pbwbc::teleoperation_messages::joint_master2slave*>(&output_buffer[1]);
    packet_out->counter = counter_++;
    packet_out->timestamp = std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count();
    packet_out->state = state;
    packet_out->velocities.at(0) = gripper_velocity;
    packet_out->velocities.fill(0.);
    ep_.send_feedback(
            output_buffer,
            sizeof(output_buffer));
}

void GripperMaster::keyboard_callback(int c) {
    if (c=='n') {
        std::lock_guard<std::mutex> lock(gripper_mutex_);
        gripper_dqd_ = -0.00005; 
    } else if (c=='m') {
        std::lock_guard<std::mutex> lock(gripper_mutex_);
        gripper_dqd_ = 0.00005; 
    } else if (c=='g') {
        std::lock_guard<std::mutex> lock(gripper_mutex_);
        gripper_grasp_ = true; 
    } else if(c==-1) {
        gripper_dqd_ = 0.;
        gripper_grasp_ = false; 
    }
}

void set_process_priority(int priority, bool sched_fifo) {
    struct sched_param param;
    param.sched_priority = priority;
    int res = pthread_setschedparam(pthread_self(),SCHED_FIFO,&param);
    if(res!=0) {
        std::stringstream str;
        str << "pthread_setschedparam failed: " << strerror(errno);
        std::cout << str.str() << std::endl;
        //throw std::runtime_error(str.str());
    }
}


} // namespace


