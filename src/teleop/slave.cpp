#include <teleop/teleop.hpp>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <arpa/inet.h>
#include <signal.h>
#include <Eigen/Dense>
#include <regex>
#include <fstream>
#include <iomanip>

namespace {
std::function<void(int)> signal_handler;
void c_signal_handler(int signal) {signal_handler(signal);}
}

namespace teleop {

CartesianSlave::CartesianSlave(std::string const & remote_addr, char channel)
    : ep_(pbwbc::TeleOpEndpoint::getInstance(remote_addr.size() ? 0 : 2323 )) {
    std::cout << "Slave initializing ... " << std::endl;
    
    log_fd_.open("/dev/shm/cartesian_slave.log");
    if(log_fd_.fail()) {
        throw std::runtime_error("Failed to open logging stream");
    }

    ctrl_log_fd_ = std::ofstream("slave_ctrl.log");
    if(ctrl_log_fd_.fail()) {
        throw std::runtime_error("Failed to open logging stream");
    }
    
    // start listener thread
    keep_running_ = true;
    packet_valid_ = false;
    
    if(remote_addr.size()) {
        std::cout << "Initiating connection to master" << std::endl;
        std::smatch regex_results;
        if(!std::regex_match(remote_addr,regex_results,std::regex("\\[(.*)\\]:([0-9]+)$"))) {
            throw std::runtime_error("Could not parse ip address");
        }
        ep_.connect(regex_results.str(1),regex_results.str(2));
        send_packet(
                pbwbc::teleoperation_messages::slave_state::STANDBY,
                std::array<double,6>(), std::array<double,6>());
    }

    channel_ = channel;

    ep_.register_channel_callback(channel_,
            [&](char channel, char *buffer, std::size_t buffer_size) {
                this->teleop_callback(channel,buffer,buffer_size);
            });

    counter_ = 0;
    
    signal_handler = [&](int signal){
        std::cout << "Ctrl+C received, shutting down" << std::endl;
        this->keep_running_ = false;
    };
    signal(SIGINT, c_signal_handler);


    startup_time_ = std::chrono::system_clock::now();
    connection_timeout_ = 0.4;

    logger_thread_ = std::thread([&](){ this->logger_thread(); });

    std::cout << "done" << std::endl;
};



CartesianSlave::~CartesianSlave(){
    std::cout << "Shutting down slave ... " << std::endl;
    keep_running_ = false;
    {
	    std::unique_lock<std::mutex> lock(log_fd_mutex_);
	    log_cv_.notify_all();
    }
    logger_thread_.join();
    std::cout << "done" << std::endl;
};


void CartesianSlave::teleop_callback(char channel, char * buffer, char buffer_size) {
    if(channel_!=channel) {
        std::cout << "Packet on unexpected channel" << std::endl;
        return;
    }
    if(buffer_size!=sizeof(pbwbc::teleoperation_messages::cartesian_master2slave)) {
        throw std::runtime_error("Corrupted feedback packet");
    }
    std::lock_guard<std::mutex> lock(packet_in_mutex_);
    typedef pbwbc::teleoperation_messages::cartesian_master2slave msg_t;
    auto new_packet = *(msg_t*)&buffer[0];
    if(packet_valid_) {
        int delta = new_packet.counter - packet_in_.counter;
        if(delta>1) {
            std::cout << "\n Packet(s) lost: " << delta-1 << std::endl;
        }else if(delta<1) {
            std::cout << "\n Packets reordered, ignoring" << std::endl;
            return;
        }
    }
    packet_in_ = *(pbwbc::teleoperation_messages::cartesian_master2slave*)&buffer[0];
    packet_valid_ = true;
    packet_in_timestamp_ = std::chrono::system_clock::now();
    std::lock_guard<std::mutex> log_lock(log_fd_mutex_);
    log_fd_ << "I " << packet_in_.counter << " " <<  std::setprecision(15) << packet_in_.timestamp << " " <<
        std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count();
    std::array<float,6> cartesian_velocity = packet_in_.cartesian_velocity;
    for(auto const & v : cartesian_velocity) {
        log_fd_ << " " << std::setprecision(6) << v;
    }
    log_fd_ << std::endl;
}

bool CartesianSlave::packet_valid() {
    std::lock_guard<std::mutex> lock(packet_in_mutex_);
    double last_packet_received = 
        std::chrono::duration<double>(
           std::chrono::system_clock::now() - packet_in_timestamp_).count();
    return packet_valid_ && (last_packet_received < connection_timeout_);
}

void CartesianSlave::send_packet(
        pbwbc::teleoperation_messages::slave_state const & state,
        std::array<double,6> const & cartesian_velocity,
        std::array<double,6> const & cartesian_wrench) {
    char output_buffer[1+sizeof(pbwbc::teleoperation_messages::cartesian_slave2master)];
    output_buffer[0] = channel_;
    pbwbc::teleoperation_messages::cartesian_slave2master *packet_out = 
        reinterpret_cast<pbwbc::teleoperation_messages::cartesian_slave2master*>(&output_buffer[1]);
    packet_out->counter = counter_++;
    packet_out->timestamp = std::chrono::duration<double>(std::chrono::system_clock::now().time_since_epoch()).count();
    packet_out->state = state;
    for(std::size_t idx=0;idx<packet_out->cartesian_wrench.size();idx++) {
        packet_out->cartesian_wrench[idx] = cartesian_wrench[idx];
    }
    ep_.send_feedback(
            output_buffer,
            sizeof(output_buffer));

    std::unique_lock<std::mutex> lock(log_fd_mutex_,std::defer_lock);
    if(lock.try_lock()) {
       packet_out_log_ = *packet_out;
       log_cartesian_velocity_ = cartesian_velocity;
       log_cv_.notify_one();
    } else {
        std::cout << "\nlogging message log\n" << std::endl;
    }
}

void CartesianSlave::logger_thread() {
    while(true) {
       std::unique_lock<std::mutex> lock(log_fd_mutex_);
       log_cv_.wait(lock);
       if(!keep_running_)return;
       log_fd_ << "O " << packet_out_log_.counter << " " <<  std::setprecision(15) << packet_out_log_.timestamp;
       std::array<float,6> cartesian_wrench = packet_out_log_.cartesian_wrench;
       for(auto const & v : cartesian_wrench) {
           log_fd_ << " " << std::setprecision(6) << v;
       }
       for(auto const & v : log_cartesian_velocity_) {
           log_fd_ << " " << std::setprecision(6) << v;
       }
       log_fd_ << std::endl;
    }
}

void CartesianSlave::wait_for_key() {
    while(keep_running_) {
        fd_set set;
        char buff[100];
        FD_ZERO(&set); /* clear the set */
        FD_SET(STDIN_FILENO, &set); /* add our file descriptor to the set */
        struct timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;
        int rv = select(STDIN_FILENO + 1, &set, NULL, NULL, &timeout);
        if(rv > 0) {
            int len = 100;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
            read( STDIN_FILENO, buff, len ); /* there was data to read */
#pragma GCC diagnostic pop
            break;
        }
    }
}

void CartesianSlave::execute() {
    std::cout << "Hit enter to move robot back to start" << std::endl;
    wait_for_key();
    if(!keep_running_)return;
    robot_startup();

    while(keep_running_) {
        std::cout << "Waiting for master to switch to FREE" << std::endl;
        while(keep_running_) {
            // TODO: stop sending when this end is receiving and the connection is stale ( > 1.s)
            if(packet_valid() && 
                    packet_in_.state==pbwbc::teleoperation_messages::master_state::FREE) {
                break;
            }
            send_packet(
                    pbwbc::teleoperation_messages::slave_state::STANDBY,
                    std::array<double,6>(), std::array<double,6>());
            std::this_thread::sleep_for (std::chrono::milliseconds(10));
        }
        if(!keep_running_)break;
        std::cout << "Master is now in FREE" << std::endl;
        std::cout << "Slave is switching to velocity control" << std::endl;
        robot_control();
        // report to master that slave is no longer running
        send_packet(
                pbwbc::teleoperation_messages::slave_state::STANDBY,
                std::array<double,6>(), std::array<double,6>());
    }
}


}


