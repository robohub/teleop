#include <teleop/teleop.hpp>
#include <iostream>
#include <iomanip>

namespace teleop {

FakeSlave::FakeSlave(std::string const & remote_addr, char channel)
    : CartesianSlave(remote_addr,channel) {}
FakeSlave::~FakeSlave(){}
void FakeSlave::robot_startup(){};
void FakeSlave::robot_control(){
    while(true) {
        if (!packet_valid() ||
                packet_in_.state!=pbwbc::teleoperation_messages::master_state::FREE ||
                    keep_running_==false) {
            std::cout << "Slave is leaving velocity control:";
            if(!packet_valid())std::cout << " Connection timeout";
            if(packet_in_.state!=pbwbc::teleoperation_messages::master_state::FREE)std::cout << " Master left FREE state";
            if(!keep_running_)std::cout << " Local termination request";
            std::cout << std::endl;
            break;
        }
        send_packet(pbwbc::teleoperation_messages::slave_state::RUNNING,
                std::array<double,6>(), std::array<double,6>());
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        std::lock_guard<std::mutex> lock(packet_in_mutex_);
        std::array<float,6> cartesian_velocity = packet_in_.cartesian_velocity;
        for(auto const & v : cartesian_velocity) {
            std::cout << "   " << std::fixed << std::right << std::showpos << std::setprecision(4) << std::setw(6) << v;
        }
        std::cout << "          \r";
    }
   packet_valid_ = false;
};

} //namespace

