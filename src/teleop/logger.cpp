#include "log4cxx/logger.h"
#include "log4cxx/basicconfigurator.h"
#include "log4cxx/helpers/exception.h"
#include <teleop/logger.hpp>

using namespace log4cxx;
using namespace log4cxx::helpers;

namespace teleop {

void configure_log4cxx() {
      BasicConfigurator::configure();
}

}


