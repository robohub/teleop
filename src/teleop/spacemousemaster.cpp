#include <teleop/teleop.hpp>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <arpa/inet.h>
#include <signal.h>
#include <termios.h>
#include <linux/socket.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/input-event-codes.h>
#include <iostream>
#include <iomanip>
#include <stdio.h>

#define TEST_BIT(bit, array) (array[bit / 8] & (1 << (bit % 8)))


namespace teleop {

SpaceMouseMaster::SpaceMouseMaster(
        std::string const & remote_addr, char channel, char gripper_channel, char joint_channel)
    : CartesianMaster(remote_addr,channel), GripperMaster(CartesianMaster::ep_,gripper_channel,joint_channel) {
    std::string spacemouse_device("/dev/input/spacemouse");
    if(getenv("SPACEMOUSE_DEVICE")) {
        spacemouse_device = std::string(getenv("SPACEMOUSE_DEVICE"));
    }
    if(access(spacemouse_device.c_str(),R_OK)==-1) {
        std::cout << 
            "\033[1;31mSpacemouse device not available\033[0m: " <<
            spacemouse_device << 
            std::endl;
        spacemouse_fd_ = -1;
    } else {
        // test device with $> evtest /dev/input/spacemouse
        spacemouse_fd_ = open(spacemouse_device.c_str(),O_RDONLY);

        // detect event device axis limits
        uint8_t abs_b[ABS_MAX/8 + 1];
        struct input_absinfo abs_feat;
        ioctl(spacemouse_fd_, EVIOCGBIT(EV_ABS, sizeof(abs_b)), abs_b);

        // report available axis
        //printf("Supported Absolute axes:\n");
        for (int yalv = 0; yalv < ABS_MAX; yalv++) {
            if (TEST_BIT(yalv, abs_b)) {
                printf("  Absolute axis 0x%02x ", yalv);
                switch ( yalv)
                {
                    case ABS_X :
                        printf("(X Axis)");
                        break;
                    case ABS_Y :
                        printf("(Y Axis)");
                        break;
                    default:
                        printf("(%d Axis)", yalv);
                }
                // ABS_X, ABS_Y, etc are defined in
                // /usr/include/linux/input-event-codes.h
                // Check there for unknown axis codes
                if(ioctl(spacemouse_fd_, EVIOCGABS(yalv), &abs_feat)) {
                    perror("evdev EVIOCGABS ioctl");
                }
                printf(" Value: %d (min:%d max:%d flat:%d fuzz:%d)",
                        abs_feat.value,
                        abs_feat.minimum,
                        abs_feat.maximum,
                        abs_feat.flat,
                        abs_feat.fuzz);
                printf("\n");
                calibration_data_[yalv] =
                    std::tuple<int,int>(abs_feat.minimum,abs_feat.maximum);
            }
        }
    }
}

SpaceMouseMaster::~SpaceMouseMaster() {}

void SpaceMouseMaster::keyboard_callback(int c) {
    GripperMaster::keyboard_callback(c);
}

void SpaceMouseMaster::robot_startup() {}

void SpaceMouseMaster::robot_control() {
    unsigned int packets_received = 0;
    double average_timestamp_difference = 0.;
    user_stop_ = false; 
    // define callback for the torque control loop
    std::array<double,6> cartesian_velocity;
    cartesian_velocity.fill(0);
    std::array<double,6> cartesian_acceleration;
    cartesian_acceleration.fill(0);

    while(true) {
        if(spacemouse_fd_!=-1) {
            // block for a maximum of 10ms on spacemouse file descriptor
            fd_set set;
            FD_ZERO(&set); /* clear the set */
            FD_SET(spacemouse_fd_, &set); /* add our file descriptor to the set */
            struct timeval timeout;
            timeout.tv_sec = 0;
            timeout.tv_usec = 10000;
            int rv = select(spacemouse_fd_ + 1, &set, NULL, NULL, &timeout);
            if(rv > 0) {
                // TODO: consume all events, only one is read now
                struct input_event buffer;
                int recv_len = read(spacemouse_fd_, &buffer, sizeof(buffer));
                if(recv_len>0) {
                    // normalize velocities to 0.1 m/s, 0.1 rad/s
                    const double translation_scaling = 0.1;
                    const double angular_scaling = 0.3;
                    double scaling;
                    if(buffer.type==EV_ABS) {
                       scaling = 1./std::get<1>(calibration_data_.at(buffer.code));
                    } else {
                        scaling = 1./350;
                    }
                    // logitech kernel driver uses EV_ABS (older spacemouse)
                    // hid kernel driver uses EV_REL
                    if(buffer.type==EV_ABS || buffer.type==EV_REL) {
                        switch(buffer.code) {
                            case ABS_Y:
                            //case REL_Y: this is implied as ABS_Y==REL_Y
                                cartesian_velocity[0] = - scaling * translation_scaling * buffer.value; break;
                            case ABS_X:
                            //case REL_X:
                                cartesian_velocity[1] = - scaling * translation_scaling * buffer.value; break;
                            case ABS_Z:
                            //case REL_Z:
                                cartesian_velocity[2] = - scaling * translation_scaling * buffer.value; break;
                            case ABS_RY:
                            //case REL_RY:
                                cartesian_velocity[3] = - scaling * angular_scaling * buffer.value; break;
                            case ABS_RX:
                            //case REL_RX:
                                cartesian_velocity[4] = - scaling * angular_scaling * buffer.value; break;
                            case ABS_RZ:
                            //case REL_RZ:
                                cartesian_velocity[5] = - scaling * angular_scaling * buffer.value; break;
                        }
                    }
                }
            }
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        Eigen::Map<Eigen::Matrix<double,6,1> > eigen_cartesian_velocity(cartesian_velocity.data());
        double gripper_dqd, workspace_scaling;
        bool gripper_grasp;
        {
            std::lock_guard<std::mutex> lock(gripper_mutex_);
            gripper_dqd = gripper_dqd_;
            gripper_grasp = gripper_grasp_;
        }
        {
            std::lock_guard<std::mutex> lock(keyboard_mutex_);
            workspace_scaling = workspace_scaling_;
        }
        eigen_cartesian_velocity *= workspace_scaling;
        gripper_dqd *= workspace_scaling;
        
        // apply workspace rotation
        eigen_cartesian_velocity.head(3) = master_workspace_rotation_ * eigen_cartesian_velocity.head(3);
        eigen_cartesian_velocity.tail(3) = master_workspace_rotation_ * eigen_cartesian_velocity.tail(3);
        // do not forward velocities to slave if clutch is disconnected
        if(clutch_disabled_) {
            cartesian_velocity.fill(0.);
            gripper_dqd = 0.;
        }
        send_packet(
                pbwbc::teleoperation_messages::master_state::FREE,
                cartesian_velocity,cartesian_acceleration);
        gripper_send_packet(pbwbc::teleoperation_messages::master_state::FREE,
                gripper_dqd, gripper_grasp);
        gripper_send_packet_joint_msg(pbwbc::teleoperation_messages::master_state::FREE, gripper_dqd, gripper_grasp);
        {
            //std::lock_guard<std::mutex> lock(packet_in_mutex_);
            //Eigen::Map<Eigen::Matrix<float,6,1> > eigen_cartesian_wrench(packet_in_.cartesian_wrench.data());
            //double force_scaling_factor = 1.;
            //double torque_scaling_factor = 0.;
            //double wrench_filter_factor = 1.;
            double timestamp_difference =
                std::chrono::duration<double>(std::chrono::system_clock::now()
              - timestamp_t(timestamp_t::duration(packet_in_.timestamp))).count();
            //// reject packets with larger jitter after initialization of average_timestamp_difference
            //// also: do not reflect forces if clutch is disconnected
            //if(packets_received < 100 || timestamp_difference < 1.3 * average_timestamp_difference || clutch_disabled_) {
            //    cartesian_wrench_filtered.head(3) += 
            //        wrench_filter_factor * (force_scaling_factor * 
            //                master_workspace_rotation_.transpose() * eigen_cartesian_wrench.head(3).cast<double>() - cartesian_wrench_filtered.head(3));
            //    cartesian_wrench_filtered.tail(3) +=
            //        wrench_filter_factor * (
            //                master_workspace_rotation_.transpose() * torque_scaling_factor * eigen_cartesian_wrench.tail(3).cast<double>() - cartesian_wrench_filtered.tail(3));
            //} else {
            //    cartesian_wrench_filtered.head(3) += 
            //        0.01 * ( - cartesian_wrench_filtered.head(3));
            //    cartesian_wrench_filtered.tail(3) +=
            //        0.01 * ( - cartesian_wrench_filtered.tail(3));
            //}
            // saturate forces to protect user
            // TODO: soft saturation of forces (self collision avoidance controller)
            //cartesian_wrench_filtered = cartesian_wrench_filtered.array().min(20.).max(-20.);
            average_timestamp_difference += 0.01 * (timestamp_difference - average_timestamp_difference);
            packets_received++;
        }
        if(CartesianMaster::counter_%1000==0) {
            std::cout << "\nAverage timestamp difference: " << average_timestamp_difference << std::endl;
        }

        // handle termination request
        // - connection lost (connection_timeout_)
        // - slave switched off (slave state)
        // - local termination request (Ctrl+C)
        bool is_packet_valid = packet_valid();
        bool is_slave_stopped =
            last_slave_state_==pbwbc::teleoperation_messages::slave_state::RUNNING
            && packet_in_.state==pbwbc::teleoperation_messages::slave_state::STANDBY;
        if (!is_packet_valid || is_slave_stopped || !keep_running_ || user_stop_) {
            std::cout << "Stopping: ";
            if(!is_packet_valid)std::cout << "Connection timeout";
            if(is_slave_stopped)std::cout << "Slave stopped";
            if(!keep_running_)std::cout << "Local termination request";
            if(user_stop_)std::cout << "User stop";
            std::cout << std::endl;
            break;
        }
        last_slave_state_ = packet_in_.state;
       
        if(CartesianMaster::counter_%10==0) { 
            std::lock_guard<std::mutex> lock(packet_in_mutex_);
            std::array<float,6> cartesian_wrench = packet_in_.cartesian_wrench;
            for(auto const & v : cartesian_wrench) {
                std::cout << "   " << std::fixed << std::right << std::showpos << std::setprecision(4) << std::setw(6) << v;
            }
            std::cout << "          \r";
        }
    }
    keyboard_thread_keep_running_ = false;
}


} // namespace

