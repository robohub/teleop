#include <teleop/teleop_franka.hpp>

// franka
#include <franka/exception.h>
#include <franka/robot.h>
#include "examples_common.h"

namespace teleop {

FrankaSlave::FrankaSlave(std::string const & remote_addr, char channel)
    : CartesianSlave(remote_addr,channel) {
    // initialize robot
    robot_.reset(new franka::Robot("franka2"));
    robot_->automaticErrorRecovery();
    robot_->setCollisionBehavior(
            {{50.0, 50.0, 50.0, 50.0, 20.0, 20.0, 20.0}}, {{50.0, 50.0, 50.0, 50.0, 20.0, 20.0, 20.0}},
            {{30.0, 30.0, 30.0, 30.0, 10.0, 10.0, 10.0}}, {{30.0, 30.0, 30.0, 30.0, 10.0, 10.0, 10.0}},
            {{20.0, 20.0, 20.0, 20.0, 20.0, 20.0}}, {{20.0, 20.0, 20.0, 20.0, 20.0, 20.0}},
            {{100.0, 100.0, 100.0, 10.0, 10.0, 10.0}}, {{100.0, 100.0, 100.0, 10.0, 10.0, 10.0}});
    robot_->setJointImpedance({{1500, 1500, 1500, 1250, 1250, 1000, 1000}});
    // set cartesian stiffness to maximum allowed by internal controller
    robot_->setCartesianImpedance({{3000, 3000, 3000, 300, 300, 300}});
    // load the kinematics and dynamics model
    robot_model_.reset(new franka::Model(robot_->loadModel()));
    //franka::RobotState initial_state = robot.readOnce();
    gripper_.reset(new franka::Gripper("franka2"));
    
    gripper_channel_ = 201;
    ep_.register_channel_callback(gripper_channel_,
            [&](char channel, char *buffer, std::size_t buffer_size) {
                this->gripper_teleop_callback(channel,buffer,buffer_size);
            });
}

FrankaSlave::~FrankaSlave(){};

void FrankaSlave::robot_startup(){
    // First move the robot to a suitable joint configuration
    std::array<double, 7> q_goal = {{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    MotionGenerator motion_generator(0.5, q_goal);
    robot_->control(motion_generator);
    if(gripper_) {
	    gripper_->stop();
	    gripper_->homing();
	    gripper_qd_ = gripper_->readOnce().width;
    } else {
    	gripper_qd_ = 0.;
    }
    gripper_grasp_ = false;
    std::cout << "Finished moving to initial joint configuration." << std::endl;
}


void FrankaSlave::gripper_teleop_callback(char channel, char * buffer, std::size_t buffer_size) {
    if(gripper_channel_!=channel) {
        std::cout << "Packet on unexpected channel" << std::endl;
        return;
    }
    if(buffer_size!=sizeof(pbwbc::teleoperation_messages::gripper_master2slave)) {
        throw std::runtime_error("Corrupted feedback packet");
    }
    std::lock_guard<std::mutex> lock(gripper_packet_in_mutex_);
    gripper_packet_in_ = *(pbwbc::teleoperation_messages::gripper_master2slave*)&buffer[0];
    gripper_packet_valid_ = true;
    gripper_packet_in_timestamp_ = std::chrono::system_clock::now();
}

bool FrankaSlave::gripper_packet_valid() {
    std::lock_guard<std::mutex> lock(gripper_packet_in_mutex_);
    double last_packet_received = 
        std::chrono::duration<double>(
           std::chrono::system_clock::now() - gripper_packet_in_timestamp_).count();
    return gripper_packet_valid_ && (last_packet_received < connection_timeout_);
}

void FrankaSlave::robot_control(){
   bool gripper_thread_keep_running = true;
   std::thread gripper_thread([&](){
       double gripper_qd = gripper_qd_;
       bool gripper_grasp = gripper_grasp_;
       while(gripper_thread_keep_running) {
           bool has_changed = false;
           {
               std::lock_guard<std::mutex> lock(packet_in_mutex_);
               if(gripper_qd!=gripper_qd_ || gripper_grasp_!=gripper_grasp) {
                   gripper_qd = gripper_qd_;
                   gripper_grasp = gripper_grasp_;
                   has_changed = true;
               }
           }
           if(has_changed && gripper_) {
               std::cout << "New gripper target " << gripper_qd << std::endl;
               try {
                   // this blocks until the goal is reached
                   if(gripper_grasp) {
                       const double gripper_force = 30.;
                       gripper_->grasp(gripper_qd-0.01,0.1,gripper_force,
                               std::max(0.,gripper_qd-0.01),0.00);
                   } else {
                       gripper_->move(gripper_qd,0.1);
                   }
               } catch ( franka::CommandException const & ex ) {
                   std::cout << "Gripper malfunction: " << ex.what() << std::endl;
               }
               std::cout << "Gripper command returned" << std::endl;
           }
           std::this_thread::sleep_for(std::chrono::milliseconds(10));
	   if(gripper_) {
		// empty UDP queue
	   	gripper_->readOnce();
	   }
       }
       std::cout << "gripper thread terminating" << std::endl;
   });
   try {
       std::array<double,6> cartesian_velocity_filtered;
       cartesian_velocity_filtered.fill(0.);
       unsigned int packets_received = 0;
       double average_timestamp_difference = 0.;
       bool last_gripper_grasp = false;

       //double avg_comp_time = 0.;
       //int comp_time_counter = 0;

       robot_->control([&](const franka::RobotState & robot_state,
                   franka::Duration period) -> franka::JointVelocities {
	   //auto start_time = std::chrono::system_clock::now();

           typedef Eigen::Map<Eigen::Matrix<double,6,1> > map6_t;
           typedef Eigen::Map<Eigen::Matrix<double,7,1> > map7_t;
           std::array<double, 42> jacobian_array =
               robot_model_->zeroJacobian(franka::Frame::kEndEffector, robot_state);
           Eigen::Map<const Eigen::Matrix<double, 6, 7> > jacobian(jacobian_array.data());
           // detect termination request:
           // - connection lost (connection_timeout_)
           // - master state
           // - local termination request (Ctrl+C)
           // let velocities decay, then terminate velocity control
           if (!packet_valid() ||
                   packet_in_.state!=pbwbc::teleoperation_messages::master_state::FREE ||
                       keep_running_==false) {
               map6_t(cartesian_velocity_filtered.data()) +=
                   0.005 * (- map6_t(cartesian_velocity_filtered.data()));
               franka::CartesianVelocities output{0.,0.,0., 0.,0.,0.};
               output.O_dP_EE = cartesian_velocity_filtered;
               franka::JointVelocities dqd{0.,0.,0.,0., 0.,0.,0.};
               map7_t(dqd.dq.data()) =
                   jacobian.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(
                           map6_t(cartesian_velocity_filtered.data()));
               if((map6_t(cartesian_velocity_filtered.data()).array().abs() < 1e-3).all()) {
                   std::cout << "Slave is leaving velocity control:";
                   if(!packet_valid())std::cout << " Connection timeout";
                   if(packet_in_.state!=pbwbc::teleoperation_messages::master_state::FREE)std::cout << " Master left FREE state";
                   if(!keep_running_)std::cout << " Local termination request";
                   std::cout << std::endl;
                   gripper_thread_keep_running = false;
                   return franka::MotionFinished(dqd);
               } else {
                   return dqd;
               }
           }
           // forward velocity input to franka controller
           double timestamp_difference;
           {
               std::lock_guard<std::mutex> lock(packet_in_mutex_);
               timestamp_difference =
                   std::chrono::duration<double>(std::chrono::system_clock::now() - packet_in_timestamp_).count();
               Eigen::Map<Eigen::Matrix<float,6,1>> v(packet_in_.cartesian_velocity.data());
               map6_t vo(cartesian_velocity_filtered.data());
               // reject packets with larger jitter after initialization of average_timestamp_difference
               if(packets_received < 100 || timestamp_difference < 1.3 * average_timestamp_difference) {
                   vo.head(3).array() += 0.2 * (v.head(3).array().cast<double>().min(10.).max(-10.) - vo.head(3).array());
                   vo.tail(3).array() += 0.2 * (v.tail(3).array().cast<double>().min(10.).max(-10.) - vo.tail(3).array());
               } else {
                   vo.head(3) += 0.005 * ( - vo.head(3));
                   vo.tail(3) += 0.005 * ( - vo.tail(3));
                   
                   //std::cout << std::chrono::duration<double>(
                   //        std::chrono::system_clock::now() - startup_time_).count() << 
                   //    " rejected packet with jitter: " << timestamp_difference << " ("
                   //    << average_timestamp_difference << ")" << std::endl;
               }
               average_timestamp_difference += 0.01 * (timestamp_difference - average_timestamp_difference);
               packets_received++;
               if(counter_%10000==0) {
                   std::cout << "Average timestamp difference: " << average_timestamp_difference << std::endl;
               }
           }
           franka::CartesianVelocities output{0.,0.,0., 0.,0.,0.};
           output.O_dP_EE = cartesian_velocity_filtered;
           // handle gripper motion
           if(gripper_packet_valid()) {
               if(!gripper_grasp_) {
                   gripper_qd_ += gripper_packet_in_.gripper_velocity;
               }
               gripper_qd_ = std::max(std::min(gripper_qd_, 0.08), 0.);
               // on key press activate grasping
               if(!gripper_grasp_ && !last_gripper_grasp && gripper_packet_in_.gripper_grasp) {
                   std::cout << "Activating force grasp" << std::endl;
                   gripper_grasp_ = true;
                   gripper_qd_ += 0.01; // ensure gripper opens after grasping
                   // deactivate grasping on key press
               } else if (gripper_grasp_ && !last_gripper_grasp && gripper_packet_in_.gripper_grasp) { 
                   gripper_grasp_ = false;
                   std::cout << "Deactivating force grasp" << std::endl;
               }
               last_gripper_grasp = gripper_packet_in_.gripper_grasp;
           }
           //{
           //    //std::lock_guard<std::mutex> lock1(packet_in_mutex_);
           //    //std::lock_guard<std::mutex> lock2(ctrl_log_fd_mutex_);
           //    ctrl_log_fd_ << "[" << packet_in_.state << "] " << 
           //        static_cast<int>(packet_in_.state) 
           //        << " " << std::chrono::duration<double>(
           //                std::chrono::system_clock::now() - startup_time_).count()
           //        << " " << timestamp_difference 
           //        << " " << packet_in_.counter;
           //    for(auto const & x : packet_in_.cartesian_velocity)ctrl_log_fd_ << " " << x;
           //    for(auto const & x : output.O_dP_EE)ctrl_log_fd_ << " " << x;
           //    ctrl_log_fd_ << std::endl;
           //}
           // limit maximum cartesian velocities
           // TODO: smooth limiter
           //map6_t(output.O_dP_EE.data()).head(3) =
           //    map6_t(output.O_dP_EE.data()).head(3).array().min(0.3).max(-0.3);
           //map6_t(output.O_dP_EE.data()).tail(3) =
           //    map6_t(output.O_dP_EE.data()).tail(3).array().min(1.5).max(-1.5);

           // Compute cartesian wrench and send packet to master
           Eigen::Map<const Eigen::Matrix<double, 7, 1> > tau_ext_hat(robot_state.tau_ext_hat_filtered.data());
           std::array<double,6> cartesian_wrench;
           Eigen::Map<Eigen::Matrix<double,6,1>> eigen_cartesian_wrench(cartesian_wrench.data());
           // tau = J^T * W -> solve for W
           // TODO: cheaper 
           eigen_cartesian_wrench = - jacobian.transpose().jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(tau_ext_hat);

           // project into joint space
           // v = J * dq
           franka::JointVelocities dqd{0.,0.,0.,0., 0.,0.,0.};
           map7_t(dqd.dq.data()) = jacobian.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(
                       map6_t(cartesian_velocity_filtered.data()));
           //eigen_cartesian_wrench.setZero();
           // TODO: check how long this takes
           send_packet(pbwbc::teleoperation_messages::slave_state::RUNNING,
                   cartesian_velocity_filtered, cartesian_wrench);
	   //avg_comp_time +=
	   //        std::chrono::duration<double>(std::chrono::system_clock::now() - start_time).count();
	   //comp_time_counter++;
	   //if(comp_time_counter%500==0) {
	   //	std::cout << "\n" << "avg_comp_time: " << avg_comp_time/comp_time_counter << std::endl;
	   //     avg_comp_time = 0.;
	   //     comp_time_counter = 0;
	   //}

           return dqd;
       });
   } catch (const franka::Exception& e) {
       std::cout << "Robot error: " << e.what() << std::endl;
       gripper_thread_keep_running = false;
       send_packet(pbwbc::teleoperation_messages::slave_state::STANDBY,
               std::array<double,6>(), std::array<double,6>());
       std::cout << "Press enter to recover" << std::endl;
       wait_for_key();
       robot_->automaticErrorRecovery();
   }
   gripper_thread.join();
   packet_valid_ = false;
}



} // namespace

