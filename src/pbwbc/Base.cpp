#include <pbwbc/Base.hpp>
#include <pbwbc/Config.hpp>

namespace pbwbc {

// reuse ros logger here to avoid having to set up appenders for log4cxx
log4cxx::LoggerPtr logger::global_logger = log4cxx::Logger::getLogger("ros");
log4cxx::LoggerPtr logger::pbwbc_logger = log4cxx::Logger::getLogger("ros.pbwbc");

ConfigMap::ConfigMap() {}

ConfigMap::~ConfigMap() {}

std::shared_ptr<ConfigMap> ConfigMap::submap(std::string const & tag) {
    throw std::runtime_error("Not implemented");
}

bool ConfigMap::list_items(std::string const & parameter, std::vector<std::string> & keys ) {
    throw std::runtime_error("Not implemented");
}

bool ConfigMap::getParam(std::string const & parameter_name, Eigen::VectorXd & variable) const {
    std::vector<double> nums;
    if(this->getParam(parameter_name,nums)) {
        variable.resize(nums.size());
        std::copy(nums.begin(),nums.end(),&variable(0));
        return true;
    } else {
        return false;
    }
}

}


