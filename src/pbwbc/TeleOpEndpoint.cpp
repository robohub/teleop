#include <pbwbc/TeleOpEndpoint.hpp>
#include <cstring>
// getaddrinfo
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
// inet_ntop
#include <arpa/inet.h>
// close
#include <unistd.h>
// ioctl
#include <sys/ioctl.h>


namespace pbwbc {


std::ostream & operator<< (std::ostream & os, TeleOpEndpoint::ChannelIndex const & idx) {
    os << static_cast<int>(idx);
    return os;
}


TeleOpEndpoint::~TeleOpEndpoint() {
    PBWBC_INFO("TeleOpEndpoint shutting down");
    keep_running_ = false;
    std::lock_guard<std::mutex> lock(master_receive_threads_mutex_);
    for(auto & connection : master_receive_threads_) {
        shutdown(std::get<0>(connection), SHUT_RDWR);
        close(std::get<0>(connection));
        std::get<2>(connection).join();
    }
    shutdown(listen_socket_, SHUT_RDWR);
    close(listen_socket_);
    if(receive_thread_.joinable()) {
        receive_thread_.join();
    }
};


TeleOpEndpoint & TeleOpEndpoint::getInstance(int port) {
    PBWBC_INFO("TeleOpEndpoint instance requested with port: " << port);
    static std::map<int,std::unique_ptr<TeleOpEndpoint>> map;
    if(map.count(port)) {
        return *map.at(port);
    }
    map[port] = std::unique_ptr<TeleOpEndpoint>(new TeleOpEndpoint(port));
    return *map.at(port);
}

void TeleOpEndpoint::register_channel_callback(char channel_code,
        callback_t const & callback) {
    {
        std::lock_guard<std::mutex> lock(channel_map_mutex_);
        channel_map_[static_cast<ChannelIndex>(channel_code)].push_back(callback);
    }
    {
        std::lock_guard<std::mutex> lock(master_receive_threads_mutex_);
        for(auto const & connection : master_receive_threads_) {
            register_feedback(std::get<0>(connection), 
                    static_cast<ChannelIndex>(channel_code));
        }
    }
}
    
void TeleOpEndpoint::register_feedback(int fd, ChannelIndex channel_code) {
    char buffer[2];
    buffer[0]=static_cast<char>(ChannelIndex::REGISTER_FEEDBACK);
    buffer[1]=static_cast<char>(channel_code);
    PBWBC_INFO("Registering feedback for " << 
            (static_cast<int>(channel_code)&0xFF))
    int res = send(fd,
            (void*)buffer,
            sizeof(buffer),
            0);
    if(res < 0) {
        PBWBC_DEBUG("sendto failed on socket " << fd << ": " 
                << strerror(errno));
        if(errno==ECONNREFUSED) {
            for(auto & entry : feedback_table_) {
                entry.second.erase(std::remove(entry.second.begin(), entry.second.end(), fd));
            }
            shutdown(fd, SHUT_RDWR);
            close(fd);
        }
    }
}

void TeleOpEndpoint::send_feedback(const char *buffer, std::size_t buffer_len) {
    ChannelIndex channel_code = static_cast<ChannelIndex>(buffer[0]);
    std::lock_guard<std::mutex> lock(feedback_table_mutex_);
    for(auto & entry : feedback_table_) {
        if(entry.first==channel_code) {
            for(auto itr = entry.second.begin();itr!=entry.second.end();) {
                auto const & fd = *itr;
                //PBWBC_INFO("Sending feedback on channel " << channel_code << " to socket ("
                //        << fd << ")")
                int res = send(fd,
                        (void*)buffer,
                        buffer_len,
                        0);
                bool deleted = false;
                if(res < 0) {
                    PBWBC_DEBUG("sendto failed on socket " << fd << ": " 
                            << strerror(errno));
                    if(errno==ECONNREFUSED || errno==EBADF) {
                        shutdown(fd, SHUT_RDWR);
                        close(fd);
                        itr = entry.second.erase(itr);
                        deleted = true;
                    }
                }
                if(!deleted)itr++;
            }
        }
    }
}

void TeleOpEndpoint::send_feedback(char channel, const char *buffer, std::size_t buffer_len) {
    const int max_buffer_size = 1500;
    if(1+buffer_len>max_buffer_size) {
        throw std::runtime_error("Packet too large");
    }
    char new_buffer[max_buffer_size];
    new_buffer[0] = channel;
    std::memcpy((void*)&new_buffer[1],(void*)&buffer[0],buffer_len);
    send_feedback(new_buffer,1+buffer_len);
}

bool TeleOpEndpoint::feedback_channel_connected(char channel_code) {
    std::lock_guard<std::mutex> lock(feedback_table_mutex_);
    for(auto const & entry : feedback_table_) {
        if(entry.first==static_cast<ChannelIndex>(channel_code)) {
            return true;
        }
    }
    return false;
}

void TeleOpEndpoint::connect(std::string const & host, std::string const & port) {
    struct sockaddr_in6 remaddr;
    memset((char *)&remaddr, 0, sizeof(remaddr));
    remaddr.sin6_family = AF_INET6;
    inet_pton(AF_INET6, host.c_str(), &(remaddr.sin6_addr));
    remaddr.sin6_port = htons(std::stoi(port));
    
    char remaddr_host_string[INET6_ADDRSTRLEN];
    inet_ntop(AF_INET6, &(remaddr.sin6_addr), remaddr_host_string, INET6_ADDRSTRLEN);
    int remaddr_port = ntohs(remaddr.sin6_port);
    PBWBC_INFO("Connecting to [" << remaddr_host_string << "]:" << remaddr_port);
    std::lock_guard<std::mutex> lock(master_receive_threads_mutex_);
    for(auto & connection : master_receive_threads_) {
        if(memcmp((void*)&std::get<1>(connection),&remaddr,sizeof(remaddr))==0) {
            PBWBC_INFO("Connection already exists")
            return;
        }
    }


    int master_socket = 0;
    if ((master_socket = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
        std::stringstream str;
        str << "Failed to open socket" << strerror(errno);
        throw std::runtime_error(str.str());
    }
    PBWBC_INFO("Connecting handled by socket ("
            << master_socket << ")")
    //int socket_priority = 6;
    //int res = setsockopt(master_socket, SOL_SOCKET, SO_PRIORITY,
    //        (void*)&socket_priority, sizeof(socket_priority));
    //if(res!=0) {
    //    perror("Failed to set socket priority (TOS)");
    //}
    // allow reuse of this socket
    int optval = 1;
    if(setsockopt(master_socket, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))<0) {
        std::stringstream str;
        str << "receive_thread: got error from setsockopt: " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    // bind socket to existing listen address
    if (bind(master_socket, (struct sockaddr *)&servaddr_, sizeof(servaddr_)) < 0) {
        std::stringstream str;
        str << "Bind to socket failed " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    // connect socket
    if(::connect(master_socket, (struct sockaddr *)&remaddr, sizeof(remaddr))<0) {
        std::stringstream str;
        str << "connect failed " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    // create thread to handle this specific UDP "connection"
    master_receive_threads_.push_back(
            std::make_tuple(
                master_socket,
                remaddr,
                std::thread(&TeleOpEndpoint::master_receive_thread, this, master_socket, remaddr)));

    // increase cpu priority of master_thread by one, the 
    // main thread is for administrative purposes.
    // TODO: check that SCHED_FIFO is correctly inherited
    struct sched_param param;
    int policy;
    int res = pthread_getschedparam(pthread_self(),&policy,&param);
    if(res!=0) {
        std::stringstream str;
        str << "pthread_getschedparam failed: " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    param.sched_priority = std::max(static_cast<int>(param.sched_priority) + 1,0);
    res = pthread_setschedparam(
            std::get<2>(master_receive_threads_.back()).native_handle(),policy,&param);
    if(res!=0) {
        std::stringstream str;
        str << "pthread_setschedparam failed: " << strerror(errno);
        std::cerr << str.str() << std::endl;
        //throw std::runtime_error(str.str());
    }

    // request all feedback topics which are required locally
    {
        std::lock_guard<std::mutex> lock(channel_map_mutex_);
        for(auto const & entry : channel_map_) {
            register_feedback(master_socket, entry.first);
        }
    }
}

TeleOpEndpoint::TeleOpEndpoint(int port) {
    PBWBC_INFO("Teleop instance startup on port " << port);
    keep_running_ = true;
    // open listen_socket_
    if ((listen_socket_ = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
        std::stringstream str;
        str << "Failed to open socket" << strerror(errno);
        throw std::runtime_error(str.str());
    }
    // set TOS class for real-time applications
    int socket_priority = 6;
    int res = setsockopt(listen_socket_, SOL_SOCKET, SO_PRIORITY,
            (void*)&socket_priority, sizeof(socket_priority));
    if(res!=0) {
        perror("Failed to set socket priority (TOS)");
    }
    // allow reuse of this socket
    // TODO: check if the port is already used to prevent hijacking by a second instance
    // Do this by first binding without SO_REUSEPORT
    int optval = 1;
    if(setsockopt(listen_socket_, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))<0) {
        std::stringstream str;
        str << "receive_thread: got error from setsockopt: " << strerror(errno);
        throw std::runtime_error(str.str());
    }

    // bind socket
    memset((char *)&servaddr_, 0, sizeof(servaddr_));
    servaddr_.sin6_family = AF_INET6;
    servaddr_.sin6_addr = IN6ADDR_ANY_INIT;
    // If remote_addr is specified use random port
    servaddr_.sin6_port = htons(port);
    if (bind(listen_socket_, (struct sockaddr *)&servaddr_, sizeof(servaddr_)) < 0) {
        std::stringstream str;
        str << "Bind to socket failed " << strerror(errno);
        throw std::runtime_error(str.str());
    }

    // get receive queue size and set buffer alert level
    // It is possible to increase the buffer size, but we want low latency here
    int buffer_size;
    socklen_t optlen = (socklen_t)sizeof(buffer_size);
    if(getsockopt(listen_socket_, SOL_SOCKET, SO_RCVBUF, &buffer_size, &optlen)<0) {
        std::stringstream str;
        str << "receive_thread: got error from getsockopt(SO_RCVBUF)): " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    buffer_bytes_alert_level_ = buffer_size / 2;

    // register debug output channel
    register_channel_callback(
            static_cast<char>(ChannelIndex::DEBUG_MESSAGE),
            [](char ,char *buffer, std::size_t len){
                PBWBC_INFO("Debug message: \""
                        << std::string(buffer,len) << "\"");
            });
  
    // create listner thread to tend to incoming packets
    // TODO: allow connections to client? 
    if(port!=0) { 
        receive_thread_ = std::thread(&TeleOpEndpoint::receive_thread, this);
    }
}

std::string socket_info(int mysocket) {
    std::stringstream str;
    struct sockaddr_in6 remaddr;
    memset((char *)&remaddr, 0, sizeof(remaddr));
    socklen_t addrlen = sizeof(remaddr);
    if(getsockname(
                mysocket,
                (struct sockaddr *)&remaddr,
                &addrlen)!=0) {
        perror("Failed to getsockname");
    }
    char remaddr_host_string[INET6_ADDRSTRLEN];
    inet_ntop(AF_INET6, &(remaddr.sin6_addr), remaddr_host_string, INET6_ADDRSTRLEN);
    int remaddr_port = ntohs(remaddr.sin6_port);
    str << "SN: [" << remaddr_host_string << "]:" << remaddr_port;
    
    memset((char *)&remaddr, 0, sizeof(remaddr));
    if(getpeername(
                mysocket,
                (struct sockaddr *)&remaddr,
                &addrlen)==-1) {
        if(errno==ENOTCONN) {
            str << " <--> **NOT CONNECTED**";
        } else {
            perror("Failed to getpeername");
        }
    } else {
        inet_ntop(AF_INET6, &(remaddr.sin6_addr), remaddr_host_string, INET6_ADDRSTRLEN);
        remaddr_port = ntohs(remaddr.sin6_port);
        str << " <--> PN: [" << remaddr_host_string << "]:" << remaddr_port;
    }
    return str.str();
}

void TeleOpEndpoint::receive_thread() {
    pthread_setname_np(pthread_self(),"recv_thrd");
    PBWBC_INFO("receive_thread: started");
    while(keep_running_) {
        struct sockaddr_in6 remaddr;
        socklen_t addrlen = sizeof(remaddr);
        const int input_buffer_size = 1500;
        char input_buffer[input_buffer_size];

        int recvlen = recvfrom(listen_socket_,
                (void*)input_buffer,
                input_buffer_size,
                0,
                (struct sockaddr *)&remaddr,
                &addrlen);

        if(!keep_running_) { // shutdown was called on socket
            return;
        }
        if(recvlen==0) {
            std::cout << "recvfrom() returned 0-length packet" << std::endl;
            continue;
        }
        if(recvlen==-1) {
            std::stringstream str;
            str << "receive_thread: got error from recvfrom: " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        char remaddr_host_string[INET6_ADDRSTRLEN];
        inet_ntop(AF_INET6, &(remaddr.sin6_addr), remaddr_host_string, INET6_ADDRSTRLEN);
        int remaddr_port = ntohs(remaddr.sin6_port);

        // check if this remaddr is already associated with a master_socket and a thread
        // In this case these packets are arriving here because the kernel received them
        // purge them from the listen socket when the connect called for the master_socket.
        std::lock_guard<std::mutex> lock(master_receive_threads_mutex_);
        bool existing_connection = false;
        for(auto itr = master_receive_threads_.begin(); itr!=master_receive_threads_.end();itr++) {
            auto & connection = *itr;
            if(memcmp((void*)&std::get<1>(connection),&remaddr,sizeof(remaddr))==0) {
                //std::cout << "Connection from ["
                //    << remaddr_host_string << "]:" << remaddr_port <<
                //    " already handled in socket " << std::get<0>(connection) << std::endl;
                handle_packet(input_buffer, recvlen, std::get<0>(connection));

                existing_connection = true;
                break;
            }
        }
        if(existing_connection)continue;
       
        // From here: create new socket and thread handling this connection 
        int master_socket = 0;
        if ((master_socket = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
            std::stringstream str;
            str << "Failed to open socket" << strerror(errno);
            throw std::runtime_error(str.str());
        }
        PBWBC_INFO("New incoming connection from [" << remaddr_host_string << "]:" <<
                remaddr_port << " handled by socket ("
                << master_socket << ")")
        //int socket_priority = 6;
        //int res = setsockopt(master_socket, SOL_SOCKET, SO_PRIORITY,
        //        (void*)&socket_priority, sizeof(socket_priority));
        //if(res!=0) {
        //    perror("Failed to set socket priority (TOS)");
        //}
        // allow reuse of this socket
        int optval = 1;
        if(setsockopt(master_socket, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval))<0) {
            std::stringstream str;
            str << "receive_thread: got error from setsockopt(SO_REUSEPORT): " << strerror(errno);
            throw std::runtime_error(str.str());
        }

        // bind socket to existing listen address
        if (bind(master_socket, (struct sockaddr *)&servaddr_, sizeof(servaddr_)) < 0) {
            std::stringstream str;
            str << "Bind to socket failed " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        // connect socket
        if(::connect(master_socket, (struct sockaddr *)&remaddr, sizeof(remaddr))<0) {
            std::stringstream str;
            str << "connect failed " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        // create thread to handle this specific UDP "connection"
        master_receive_threads_.push_back(
                std::make_tuple(
                    master_socket,
                    remaddr,
                    std::thread(&TeleOpEndpoint::master_receive_thread, this, master_socket, remaddr)));

        // increase cpu priority of master_thread by one, the 
        // main thread is for administrative purposes.
        // TODO: check that SCHED_FIFO is correctly inherited
        struct sched_param param;
        int policy;
        int res = pthread_getschedparam(pthread_self(),&policy,&param);
        if(res!=0) {
            std::stringstream str;
            str << "pthread_getschedparam failed: " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        param.sched_priority = std::max(static_cast<int>(param.sched_priority) + 1,0);
        res = pthread_setschedparam(
                std::get<2>(master_receive_threads_.back()).native_handle(),policy,&param);
        if(res!=0) {
            std::stringstream str;
            str << "pthread_setschedparam failed: " << strerror(errno);
            std::cerr << str.str() << std::endl;
            //throw std::runtime_error(str.str());
        }

        // handle the contents of this packet
        handle_packet(input_buffer, recvlen, master_socket);
        // request all feedback topics which are required locally
        {
            std::lock_guard<std::mutex> lock(channel_map_mutex_);
            for(auto const & entry : channel_map_) {
                register_feedback(master_socket, entry.first);
            }
        }
    }
    PBWBC_INFO("receive_thread: terminating");
}

void TeleOpEndpoint::handle_packet(char *buffer, std::size_t len, int master_socket) {
    if(len==0) {
        throw std::runtime_error("handle_packet: 0-length packet");
    }
    ChannelIndex channel_index = static_cast<ChannelIndex>(buffer[0]);
    if(channel_index==ChannelIndex::REGISTER_FEEDBACK) {
        if(len!=2) {
            throw std::runtime_error("Corrupted REGISTER_FEEDBACK packet");
        }
        ChannelIndex feedback_channel = static_cast<ChannelIndex>(buffer[1]);
        PBWBC_INFO("received REGISTER_FEEDBACK from (" << master_socket
                << ") for channel " << (static_cast<int>(feedback_channel)&0xFF));
        std::lock_guard<std::mutex> lock(feedback_table_mutex_);
        // register this socket for the channel, but only once
        auto & info = feedback_table_[feedback_channel];
        if(std::count(info.begin(),info.end(),master_socket)==0) {
            info.push_back(master_socket);
        }
        // TODO: Implement individual TOS/QOS classes for different feedback channels
        // This requires having a send only socket with specific TOS set through setsockopt.
        // This can be done by adding another field to the REGISTER_FEEDBACK message which
        // sets the priority.
        // If the priority is not the default one
        // the code then clones the connection / fd and adds the second fd feedback_table
        // without starting a master_receive_thread (otherwise two threads receive on the same
        // connection, actually this will not create any problems). Maybe.
        // This strategy needs testing to see how connection loss is handled.
    } else {
        // dispatch packet to registered interfaces
        std::lock_guard<std::mutex> lock(channel_map_mutex_);
        if(channel_map_.count(channel_index)) {
            //PBWBC_INFO("Dispatching message for channel "
            //        << channel_index << " to callback");
            for(auto const & callback : channel_map_.at(channel_index)) {
                callback(static_cast<char>(channel_index),buffer+1,len-1);
            }
        } else {
            PBWBC_FATAL("Got packet with unregistered ChannelIndex: "
                    << static_cast<int>(channel_index));
        }
    }
}

void TeleOpEndpoint::master_receive_thread(int master_socket, struct sockaddr_in6 remaddrX) {
    pthread_setname_np(pthread_self(),"mrecv_thrd");
    PBWBC_DEBUG("TeleOpEndpoint::master_receive_thread("
            << master_socket
            << "): started");

    while(keep_running_) {
        const int input_buffer_size = 1500;
        char input_buffer[input_buffer_size];

        //PBWBC_INFO("master_receive_thread recvfrom on socket (" << master_socket << ") with " << socket_info(master_socket))
        int recvlen = recv(master_socket,
                (void*)input_buffer,
                input_buffer_size,
                0);

        //PBWBC_INFO("master_receive_thread recvfrom on socket (" << master_socket << ") returned")
        if(!keep_running_) { // shutdown was called on socket
            break;
        }
        if(recvlen==0) {
            PBWBC_INFO("master_receive_thread: recvfrom() returned 0-length packet")
            continue;
        }
        if(recvlen==-1) {
            PBWBC_INFO("TeleOpEndpoing::master_receive_thread("
                    << master_socket
                    << "): got error from recvfrom: " << strerror(errno));
            if(errno==ECONNREFUSED || errno==EBADF) {
                // remove feedback channels active for this master
                std::lock_guard<std::mutex> lock(feedback_table_mutex_);
                for(auto & entry : feedback_table_) {
                    entry.second.erase(std::remove(entry.second.begin(), entry.second.end(), master_socket), entry.second.end());
                }
                // terminate this thread, thread will be joined at shutdown
                break;
            }
        }
        
        // alert user to high buffer levels
        int buffer_bytes;
        if(ioctl(master_socket, FIONREAD, &buffer_bytes)==-1) {
            std::stringstream str;
            str << "master_receive_thread: got error from ioctl: " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        if(buffer_bytes > buffer_bytes_alert_level_) {
            PBWBC_INFO("master_receive_thread: (" << master_socket << 
                    ") high buffer level alert: "  << buffer_bytes);
        }
        
        //PBWBC_INFO("handle_packet() for socket (" << master_socket << ")");
        handle_packet(input_buffer,recvlen,master_socket);
    }
    PBWBC_DEBUG("master_receive_thread("
            << master_socket
            << "): terminating");
}


} // namespace


