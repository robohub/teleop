#include <pbwbc/TeleOpProxy.hpp>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <regex>

namespace pbwbc {

TeleOpProxy::TeleOpProxy(std::string const & remote_addr, int udp_port, char channel
       )
    : ep_(TeleOpEndpoint::getInstance(remote_addr.size() ? 0 : 2323))
{
    PBWBC_DEBUG("TeleOpProxy starting");
   
    if(remote_addr.size()) {
        std::smatch regex_results;
        if(!std::regex_match(remote_addr,regex_results,std::regex("\\[(.*)\\]:([0-9]+)$"))) {
            throw std::runtime_error("Could not parse ip address");
        }
        ep_.connect(regex_results.str(1),regex_results.str(2));
    }

    //int channel;
    //if(!config->getParam("channel", channel)) {
    //    // Not usable without this parameter
    //    throw std::runtime_error("Need channel parameter");
    //}
    PBWBC_DEBUG("TeleOpProxy operating on channel " << (static_cast<int>(channel)&0xFF) );
    channel_ = channel;
    
    // open socket
    if ((socket_ = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
        std::stringstream str;
        str << "Failed to open socket" << strerror(errno);
        throw std::runtime_error(str.str());
    }
    
    // bind socket
    struct sockaddr_in6 servaddr;
    memset((char *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin6_family = AF_INET6;
    servaddr.sin6_addr = IN6ADDR_ANY_INIT;
    servaddr.sin6_port = htons(udp_port);
    if (bind(socket_, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        std::stringstream str;
        str << "Bind to socket failed " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    
    // get receive queue size and set buffer alert level
    // It is possible to increase the buffer size, but we want low latency here
    int buffer_size;
    socklen_t optlen = (socklen_t)sizeof(buffer_size);
    if(getsockopt(socket_, SOL_SOCKET, SO_RCVBUF, &buffer_size, &optlen)<0) {
        std::stringstream str;
        str << "receive_thread: got error from getsockopt(SO_RCVBUF)): " << strerror(errno);
        throw std::runtime_error(str.str());
    }
    buffer_bytes_alert_level_ = buffer_size / 2;
    
    counter_ = 0; 
    keep_running_ = true;
    if(udp_port!=0) { // listening on UDP port, forwarding to TeleOpEndpoint
        PBWBC_DEBUG("TeleOpProxy listening for packets on UDP port " << udp_port);
        receive_thread_ = std::thread(&TeleOpProxy::receive_thread, this);
    } else { // listing on TeleOpEndpoint channel, forwarding to udp port
        PBWBC_DEBUG("TeleOpProxy sending packets to [::1]:2325");
        std::string host("::1");
        std::string port("2325");
        struct sockaddr_in6 remaddr;
        memset((char *)&remaddr, 0, sizeof(remaddr));
        remaddr.sin6_family = AF_INET6;
        inet_pton(AF_INET6, host.c_str(), &(remaddr.sin6_addr));
        remaddr.sin6_port = htons(std::stoi(port));
        
        if(connect(socket_, (struct sockaddr *)&remaddr, sizeof(remaddr))<0) {
            std::stringstream str;
            str << "connect failed " << strerror(errno);
            throw std::runtime_error(str.str());
        }

        ep_.register_channel_callback(channel_,[&](char channel, char * buffer, std::size_t size) {
                this->teleop_callback(channel, buffer,size);
           });
    }
}

TeleOpProxy::~TeleOpProxy() {
    PBWBC_DEBUG("TeleOpProxy shutdown");
    keep_running_ = false;
    shutdown(socket_, SHUT_RDWR);
    close(socket_);
    if(receive_thread_.joinable()) {
        receive_thread_.join();
    }
}

void TeleOpProxy::teleop_callback(char channel, char * buffer, std::size_t size) {
    if(channel_!=channel) {
        PBWBC_INFO("TeleOpProxy: callback for unexpected channel");
        return;
    }
    uint64_t counter = *reinterpret_cast<uint64_t*>(&buffer[0]);
    PBWBC_INFO("counter: " << counter);
    if(counter_+1!=counter) {
        PBWBC_INFO("TeleOpProxy: counter jumped. Expected " << counter_+1 << " got " << counter);
        counter_ = counter;
    } else {
        counter_++;
    }

    /*int res = */send(socket_,
            static_cast<void*>(buffer+sizeof(uint64_t)),
            size,
            0);
    // Ignoring errors here, user may start/restart receiver later
    //if(res < 0) {
    //    PBWBC_DEBUG("TeleOpProxy: sendto failed on socket " << socket_ << ": " 
    //            << strerror(errno));
    //}
}


void TeleOpProxy::receive_thread() {
    while(keep_running_) {
        struct sockaddr_in6 remaddr;
        socklen_t addrlen = sizeof(remaddr);
        const int input_buffer_size = 1500;
        char input_buffer[input_buffer_size];
        input_buffer[0] = channel_;
        *reinterpret_cast<uint64_t*>(&input_buffer[1]) = counter_;
        counter_++;

        int recvlen = recvfrom(socket_,
                (void*)&input_buffer[1+sizeof(uint64_t)],
                input_buffer_size-1-sizeof(uint64_t),
                0,
                (struct sockaddr *)&remaddr,
                &addrlen);

        if(!keep_running_) { // shutdown was called on socket
            return;
        }
        if(recvlen==0) {
            std::cout << "recvfrom() returned 0-length packet" << std::endl;
            continue;
        }
        if(recvlen==-1) {
            std::stringstream str;
            str << "receive_thread: got error from recvfrom: " << strerror(errno);
            throw std::runtime_error(str.str());
        }
        
        // alert user to high buffer levels
        int buffer_bytes;
        if(ioctl(socket_, FIONREAD, &buffer_bytes)==-1) {
            PBWBC_INFO("ioctl failed");	
        }

        if(buffer_bytes > buffer_bytes_alert_level_) {
            PBWBC_INFO("TeleOpProxy: high buffer level alert: "  << buffer_bytes);
        }
        ep_.send_feedback(input_buffer, 1+sizeof(uint64_t)+recvlen);
    }
}


} // namespace pbwbc
