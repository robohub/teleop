#ifndef PBWBC_TELEOPENDPOINT_HPP
#define PBWBC_TELEOPENDPOINT_HPP

#if BUILDING_TELEOP
#define TELEOP_EXPORT __attribute__((__visibility__("default")))
#else
#define TELEOP_EXPORT
#endif
#include <pbwbc/Base.hpp>
#include <thread>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>

namespace pbwbc {

// This class provides a non-ROS interface for controlling the
// the robot in a continues way. The main use case is teleoperation.
// A single UDP connection to receive commands and provide feedback.
//
// It allows multiple connections (from several masters) controlling
// various portions of the robot. For this each package starts with
// a channel_code (single byte).
//
// The masters can also subscribe to feedback channels by sending a special
// message. See examples for help.
//
// This class does not handle serialization of the messages, it just
// forwards buffers
//
// Print debug message in TeleOpEndpoint
// echo -e -n "\x00Hello World" | nc -u ::1 2323
//
// Register for feedback on channel 0
// echo -e -n "\x01\x00" | nc -u ::1 2323
class TELEOP_EXPORT TeleOpEndpoint {
public:
    ~TeleOpEndpoint();
    TeleOpEndpoint(TeleOpEndpoint const&)  = delete;
    void operator=(TeleOpEndpoint const&)  = delete;

    // Get a singleton instance of this for a given port number.
    // If port==0 this instance is a client which cannot receive incoming packets
    // from unknown endpoints. Then all connections must be created through connect()
    //
    // Using a singleton allows access to the instance from distant portions of the
    // code without passing a reference through many api layers
    static TeleOpEndpoint & getInstance(int port);

    enum class ChannelIndex : char {
    DEBUG_MESSAGE = 0,
    REGISTER_FEEDBACK = 1,
    FIRST_USER_CHANNEL = 10
    };

    // The user code (e.g. controller / trajectory generator) calls this method to
    // register a callback for a specific channel. The callback will be activated
    // when any connected endpoint send a packet for the specific channel
    // This method populates channel_map_
    // TODO: need method to deregister callback when user code is deinitialized
    // The TeleOpEndpoint instance is initialized on process exit
    // Idea: return a std::unique_ptr<CallbackHandle>
    typedef std::function<void(char,char*,std::size_t)> callback_t;
    void register_channel_callback(char channel_code,
            callback_t const & callback);

    // This function allows the controller to provide feedback. This method
    // checks feedback_table_ and then sends out the packet to the corresponding
    // master. The user has to prefix the content with the one byte channel index
    void send_feedback(const char *buffer, std::size_t buffer_len);

    // Convenience wrapper which prefixes the content with the channel code,
    // at the cost of a memcpy
    void send_feedback(char channel_code, const char *buffer, std::size_t buffer_len);
   
    // Check if a certain feedback channel has a subscriber
    bool feedback_channel_connected(char channel_code);

    // This function looks up the hostname with getaddrinfo and the
    // sends a first packet to this address. 
    void connect(std::string const & hostinfo, std::string const & port);

    // TODO: Register for events on channel (Remote side requests/disconnected)
    //void register_event_callback(char channel_code,
    //        std::function<void(char)> const &);
protected:
    TeleOpEndpoint(int port);

    bool keep_running_;
    struct sockaddr_in6 servaddr_;
    int listen_socket_;
    int buffer_bytes_alert_level_;
    // This functions handles new connections, creating
    // a separate socket and calling connect() and
    // then starting a thread (master_receive_thread) to
    // handle this connection
    void receive_thread();
    std::thread receive_thread_;

    std::map<ChannelIndex,std::vector<callback_t>> channel_map_;
    std::mutex channel_map_mutex_;

    // Handle incoming packet, called by receive_thread()
    // and master_receive_thread()
    void handle_packet(char *, std::size_t len, int socket);

    // This functions handles a single master connection,
    // issues callbacks when recieving a packet.
    // The master can register feedback on a given channel
    // using a special command
    void master_receive_thread(int socket, struct sockaddr_in6 remaddrX);

    // This stores the socket and the thread variable for termination
    // purposes
    std::vector<std::tuple<int,struct sockaddr_in6,std::thread> > master_receive_threads_;
    std::mutex master_receive_threads_mutex_;

    // This maps from a channel_index (feedback) to a list of
    // file descriptors to recieve the update
    std::map<ChannelIndex,std::vector<int>> feedback_table_;
    std::mutex feedback_table_mutex_;

    // Subscribe to a feedback channel with REGISTER_FEEDBACK
    // called from register_channel_callback and
    // from receive_master (from new connections)
    void register_feedback(int socket, ChannelIndex channel_code);

};

// Messages for Cartesian teleoperation
namespace teleoperation_messages {

enum class master_state : char {
    STANDBY,
    STARTING_FREE,
    FREE,
    STARTING_FEEDBACK,
    FEEDBACK,
    STOPPING
};

enum class slave_state : char {
    UNINITIALIZED,
    STANDBY,
    STARTING,
    RUNNING,
    MOVE_BACK
};

class __attribute__((__packed__)) cartesian_master2slave {
public:
    int counter;
    double timestamp;
    master_state state;
    std::array<float,6> cartesian_velocity;
    std::array<float,6> cartesian_acceleration;
};

class __attribute__((__packed__)) cartesian_slave2master {
public:
    int counter;
    double timestamp;
    slave_state state;
    std::array<float,6> cartesian_wrench;
};
          
class __attribute__((__packed__)) joint_master2slave {
public:
    int counter;
    double timestamp;
    master_state state;
    std::array<float,7> velocities;
};

class __attribute__((__packed__)) joint_slave2master {
public:
    int counter;
    double timestamp;
    slave_state state;
    std::array<float,7> torques;
};


class __attribute__((__packed__)) gripper_master2slave {
public:
    int counter;
    double timestamp;
    master_state state;
    bool gripper_grasp;
    float gripper_velocity;
};

class __attribute__((__packed__)) gripper_slave2master {
public:
    int counter;
    double timestamp;
    slave_state state;
    float gripper_qd;
    float gripper_force;
};



}

} // namespace


#endif // PBWBC_TELEOPENDPOINT_HPP
