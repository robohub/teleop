#ifndef PBWBC_TELEOPPROXY_HPP
#define PBWBC_TELEOPPROXY_HPP

#include <pbwbc/TeleOpEndpoint.hpp>
#include <pbwbc/Base.hpp>
#include <pbwbc/Config.hpp>

namespace pbwbc {

//! \brief TeleOpProxy tunnels a UDP connection through the
//!        TeleOpEndpoint connection. It ingests the UDP packages
//!        on a local port and forwards it over a specified channel
//!
class TELEOP_EXPORT TeleOpProxy {
    public:
        TeleOpProxy(std::string const & remote_addr, int udp_port, char channel);
        virtual ~TeleOpProxy();

    protected:
        char channel_;

        int socket_;
        int buffer_bytes_alert_level_;
        uint64_t counter_;

        TeleOpEndpoint & ep_;

        bool keep_running_;
        std::thread receive_thread_;
        void receive_thread();

        void teleop_callback(char channel, char *buffer, std::size_t buffer_size);
};

} // namespace pbwbc

#endif // PBWBC_TELEOPPROXY_HPP
