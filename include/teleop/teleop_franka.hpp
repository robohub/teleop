#pragma once
#include <teleop/teleop.hpp>

//franka
#include <franka/robot.h>
#include <franka/model.h>
#include <franka/gripper.h>

namespace teleop {

class TELEOP_EXPORT FrankaMaster : public CartesianMaster, public GripperMaster {
public:
    FrankaMaster(std::string const & remote_addr, char channel, char gripper_channel, char joint_channel);
    virtual ~FrankaMaster();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    std::shared_ptr<franka::Robot> robot_;
    std::shared_ptr<franka::Model> robot_model_;
    
    void robot_init();
    void robot_startup();
    void robot_control();

    void keyboard_callback(int c);
};

class TELEOP_EXPORT FrankaSlave : public CartesianSlave {
public:
    FrankaSlave(std::string const & remote_addr, char channel);
    virtual ~FrankaSlave();

protected:
    virtual void robot_startup();
    virtual void robot_control();
    std::shared_ptr<franka::Robot> robot_;
    std::shared_ptr<franka::Model> robot_model_;
    std::shared_ptr<franka::Gripper> gripper_;

    bool gripper_grasp_;
    double gripper_qd_;
    void gripper_teleop_callback(char channel, char * buffer, std::size_t buffer_size);
    
    int gripper_counter_;
    char gripper_channel_;

    bool gripper_packet_valid();
    
    std::mutex gripper_packet_in_mutex_;
    pbwbc::teleoperation_messages::gripper_master2slave gripper_packet_in_;
    bool gripper_packet_valid_;
    timestamp_t gripper_packet_in_timestamp_;
};

} // namespace


