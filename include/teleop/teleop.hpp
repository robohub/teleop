#pragma once
#include <sys/time.h>
#include <chrono>
#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <functional>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mutex>
#include <thread>
#include <condition_variable>

#if BUILDING_TELEOP
#define TELEOP_EXPORT __attribute__((__visibility__("default")))
#else
#define TELEOP_EXPORT
#endif

#include <Eigen/Dense>

#include <pbwbc/TeleOpEndpoint.hpp>

namespace teleop {

typedef std::chrono::time_point<std::chrono::system_clock,
            std::chrono::duration<double>> timestamp_t;


class TELEOP_EXPORT CartesianMaster {
public:
    CartesianMaster(std::string const & remote_addr, char channel);
    virtual ~CartesianMaster();

    void execute();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    pbwbc::TeleOpEndpoint & ep_;

    bool keyboard_thread_keep_running_;
    pbwbc::teleoperation_messages::slave_state last_slave_state_;

    bool packet_valid();
    double connection_timeout_;

    void wait_for_key();
    
    void send_packet(
            pbwbc::teleoperation_messages::master_state const & state,
            std::array<double,6> const & cartesian_velocity,
            std::array<double,6> const & cartesian_acceleration);

    std::mutex packet_in_mutex_;
    pbwbc::teleoperation_messages::cartesian_slave2master packet_in_;
    bool packet_valid_;
    char channel_;
    timestamp_t packet_in_timestamp_;

    std::mutex log_fd_mutex_;
    std::ofstream log_fd_;
    
    std::mutex keyboard_mutex_;
    bool clutch_disabled_;
    bool user_stop_;
    double workspace_scaling_;

    Eigen::Matrix<double,3,3> master_workspace_rotation_;

    int counter_;
    bool keep_running_;

    void teleop_callback(char channel, char * buffer, char buffer_size);

    virtual void robot_startup() = 0;
    virtual void robot_control() = 0;

    virtual void keyboard_callback(int c);
};


// Allows keyboard input to drive things, e.g. gripper
class TELEOP_EXPORT KeyboardMaster {
    KeyboardMaster(std::string const & remote_addr, char channel);
    virtual ~KeyboardMaster();

    // TODO: check design: This will block here
    // if CartesianMaster::execute and KeyboardMaster::execute run in different thread keyboard
    // must be multiplexed somehow
    void execute();
protected:
    pbwbc::TeleOpEndpoint & ep_;

};


class TELEOP_EXPORT CartesianSlave {
public:
    CartesianSlave(std::string const & remote_addr, char channel);
    virtual ~CartesianSlave();

    void execute();
protected:
    pbwbc::TeleOpEndpoint & ep_;

    void send_packet(
            pbwbc::teleoperation_messages::slave_state const & state,
            std::array<double,6> const & cartesian_velocity,
            std::array<double,6> const & cartesian_wrench);
    int counter_;
    char channel_;

    bool packet_valid();
    double connection_timeout_;
    
    void teleop_callback(char channel, char * buffer, char buffer_size);

    void wait_for_key();
    bool keep_running_;
    std::thread logger_thread_;
    pbwbc::teleoperation_messages::cartesian_slave2master packet_out_log_;
    std::array<double,6> log_cartesian_velocity_;
    
    void logger_thread();
    std::condition_variable log_cv_;

    std::mutex packet_in_mutex_;
    pbwbc::teleoperation_messages::cartesian_master2slave packet_in_;
    bool packet_valid_;
    timestamp_t packet_in_timestamp_;

    std::mutex log_fd_mutex_;
    std::ofstream log_fd_;
    
    std::mutex ctrl_log_fd_mutex_;
    std::ofstream ctrl_log_fd_;
    
    timestamp_t startup_time_;

    virtual void robot_startup() = 0;
    virtual void robot_control() = 0;
};

class TELEOP_EXPORT FakeSlave : public CartesianSlave {
public:
    FakeSlave(std::string const & remote_addr, char channel);
    virtual ~FakeSlave();

protected:
    virtual void robot_startup();
    virtual void robot_control();
};

class TELEOP_EXPORT GripperMaster {
public:
    GripperMaster(pbwbc::TeleOpEndpoint & ep, char gripper_channel, char joint_channel);
    virtual ~GripperMaster();

protected:
    pbwbc::TeleOpEndpoint & ep_;
    char gripper_channel_;
    void gripper_send_packet(
            pbwbc::teleoperation_messages::master_state const & state,
            double gripper_velocity,
            bool gripper_grasp);
    char joint_channel_;
    void gripper_send_packet_joint_msg(
            pbwbc::teleoperation_messages::master_state const & state,
            double gripper_velocity,
            bool gripper_grasp);

    std::mutex gripper_mutex_;
    int counter_;
    double gripper_dqd_;
    bool gripper_grasp_;
    void keyboard_callback(int c);
};

class TELEOP_EXPORT SpaceMouseMaster : public CartesianMaster, public GripperMaster {
public:
    SpaceMouseMaster(std::string const & remote_addr, char channel, char gripper_channel, char joint_channel);
    virtual ~SpaceMouseMaster();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
protected:
    int spacemouse_fd_;
    
    void robot_init();
    void robot_startup();
    void robot_control();

    std::map<int, std::tuple<int,int> > calibration_data_;
    void keyboard_callback(int c);
};

void TELEOP_EXPORT set_process_priority(int priority, bool sched_fifo = true);

} // namespace


