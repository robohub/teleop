This repository contains a minimal teleoperation implementation to drive
cartesian robots over a single UDP connection. 

The software is composed of the following elements:
- Interprocess communication over UDP pbwbc::TeleOpEndpoint in include/pbwbc/TeleOpEndpoint.hpp
- Generic Master implementation teleop::CartesianMaster in include/teleop/teleop.hpp
- Generic Slave implementation teleop::CartesianSlave in include/teleop/teleop.hpp
- Gripper Master implementation teleop::GripperMaster in include/teleop/teleop.hpp

Robot/Machine specific implementations:
- Master for Franka-Emika Panda: teleop::FrankaMaster in include/teleop/teleop_franka.hpp 
- Slave for Franka-Emika Panda: teleop::SlaveMaster in include/teleop/teleop_franka.hpp 
- Master for Spacemouse/Joystick device: teleop::SpaceMouseMaster in include/teleop/teleop.hpp 
- fake slave (just display): teleop::FakeSlave in include/teleop/teleop.hpp

Applications to run:
- frankamaster in tests/frankamaster.cpp
- frankaslave in tests/frankaslave.cpp
- spacemousemaster in tests/spacemousemaster.cpp
- fakeslave in tests/fakeslave.cpp

## Usage

### Run a test setup with spacemousemaster and fakeslave

1. Terminal
```
$ ./fakeslave 
Slave initializing ... 
done
Hit enter to move robot back to start
[HIT ENTER]
Waiting for master to switch to FREE
Master is now in FREE
Slave is switching to velocity control
Slave is leaving velocity control: Master left FREE state
Waiting for master to switch to FREE
Master is now in FREE
Slave is switching to velocity control
   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000  <-- desired velocity from master
```
The slave in listening per default on the loopback interface on port 2323.

2. Terminal
Optional: Find the Spacemouse/Joystick device:
```
ls -l /dev/input/by-id
$ ls -1 /dev/input/by-id
...
usb-3Dconnexion_SpaceMouse_Compact-event-if00
...
$ export SPACEMOUSE_DEVICE=/dev/input/by-id/usb-3Dconnexion_SpaceMouse_Compact-event-if00
```
Run `spacemousemaster` which connects to `fakeslave`
```
$ ./spacemousemaster [::1]:2323
Got slave address: [::1]:2323
Master initializing ... 
Initiating connection to slave
done
Spacemouse device not available  <--- could not open device
Hit enter to move robot back to start
[HIT ENTER]
Waiting for slave to switch to STANDBY
Slave is now in STANDBY, switching master to FREE
   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000 <--- reported wrench from slave
...
```

The first argument is the IP address of the other side. For IPv4 addresses this can look like:
`[::FFFF:129.97.71.27]:5901`
for the IP address 129.97.71.27 and the port number 5901. For IPv6 just insert the address
between the brackets.

FAQ:
- Q: You have a spacemouse/joystick attached and it cannot be opened
  A: This is likely a permissions problem, have a look if you have permissions to open the device file.



### Spacemouse master
1. To find the correct space mouse device:
```
$ ls -l /dev/input/by-id
....
usb-3Dconnexion_SpaceNavigator-event-joystick
....
```
2. Set the environment variable SPACEMOUSE_DEVICE
```
$ export SPACEMOUSE_DEVICE=/dev/input/by-id/usb-3Dconnexion_SpaceNavigator-event-joystick
```
3. Start the program
```
$ ./spacemousemaster [::1]:2323
```


### Master keyboard interface
The specific master implementations can trigger certain functionality through pressing keys
- User stop: press `e` to abort the teleoperation
- Clutch: press `Space` to temporarily disconnect master and slave to allow repositioning of the master
- Workspace scaling: press  ....
- Gripper operation: press ...


### Network simulation
On labwork3 (master):
```
tc qdisc del dev eno2 root
tc qdisc add dev eno2 root handle 1: prio
tc qdisc add dev eno2 parent 1:3 handle 30: netem delay 25ms
tc filter add dev eno2 protocol ipv6 parent 1:0 prio 3 u32 match ip6 sport 2323 0xffff flowid 1:3
```

On labwork4 (slave):
```
tc qdisc del dev eno2 root
tc qdisc add dev eno2 root handle 1: prio
tc qdisc add dev eno2 parent 1:3 handle 30: netem delay 25ms
tc filter add dev eno2 protocol ipv6 parent 1:0 prio 3 u32 match ip6 dport 2323 0xffff flowid 1:3
tc filter del dev eno2 protocol ipv6 parent 1:0 prio 3 u32 match ip6 dport 2323 0xffff flowid 1:3
```

### Video link
It's possible to tunnel a video connection through the same UDP connection.

TODO: port this from v1 into a separate application (master side) and using same application (slave). This is implemented in pbwbc as pbwbc::TeleOpProxy

Best success with gstreamer so far, see below for commands.
Utilities:
- gst-inspect-1.0
- gst-device-discover
- environment variable GST_DEBUG=2

TODO:
- packet loss disrupts video stream

#### Capture stream and send via UDP
```
gst-launch-1.0 -v v4l2src device=/dev/video4 ! video/x-raw,format=YUY2,width=1920,height=1080,pixel-aspect-ratio=1/1,framerate=30/1 ! videoconvert ! x264enc tune=zerolatency pass=5 quantizer=5 speed-preset=6 key-int-max=100 bitrate=4000 ! rtph264pay mtu=1200 ! udpsink host=::1 port=2324
```
Tuneables: `quantizer` and `bitrate`


VP9:
```
gst-launch-1.0 -v v4l2src device=/dev/video4 ! video/x-raw,format=YUY2,width=1920,height=1080,pixel-aspect-ratio=1/1,framerate=30/1 ! videoconvert ! vp9enc cpu-used=5 deadline=1 error-resilient=partitions ! rtpvp9pay mtu=1200 ! udpsink host=::1 port=2324
```


#### Recieve stream via UDP and display stream 
With H264
```
gst-launch-1.0 -e -v udpsrc address=::1 port=2325 ! application/x-rtp,encoding-name=H264,payload=26 ! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink
```

VP9
```
gst-launch-1.0 -e -v udpsrc address=::1 port=2325 ! "application/x-rtp, payload=127, encoding-name=VP9" ! rtpvp9depay ! vp9dec ! videoconvert ! autovideosink
```


### Logging
The CartesianMaster and CartesianSlave instances open log files with corresponding names `/dev/shm/cartesian_XXXX.log` in the current working directory. The file format looks like this:
```
...
I 523 1624388981.21323 1624388981.21345
O 206 1624388981.21751
O 207 1624388981.2279
I 524 1624388981.23357 1624388981.23381
O 208 1624388981.23833
O 209 1624388981.24866
I 525 1624388981.25398 1624388981.25423
O 210 1624388981.25907
O 211 1624388981.26945
I 526 1624388981.27439 1624388981.27467
...
```
A line starting with `I` is created for an incoming packet, a line with `O` for an outgoing one. The next column indicates the packet counter followed by the unix timestamp in seconds and the current system time when the packet is recieved (only for incoming packets). Note that the packet counter is not unique in some cases, e.g. when a master connects multiple times to the same slave.

Check if the time on your computer is synchronized:
```
$ timedatectl status
                      Local time: Tue 2021-06-22 15:32:14 EDT
                  Universal time: Tue 2021-06-22 19:32:14 UTC
                        RTC time: Tue 2021-06-22 19:32:14
                       Time zone: America/Toronto (EDT, -0400)
       System clock synchronized: yes <-----
systemd-timesyncd.service active: yes
                 RTC in local TZ: no
```


### IPSec
To authenticate & encrypt the connection IPSec can be used.

Note: This works, but can be complicated. Especially it requires root access to set up the
connection once. However X.509 certs are actually needed in both cases to make this work properly.
What is missing for IPSec is to have a generic configuration which can be told:
Encrypt connections to this host, where the host is dynamically set. Pre-issued certs
are then used to establish the connection.

Minimal instructions for the IPSec setup:

1. Install strongswan
```
sudo apt install -y strongswan
```

2. Configure key setup
Add this snippet to /etc/ipsec.conf
```
conn ipv6-p1-p2
        connaddrfamily=ipv6       # Important for IPv6!
        left=IP_ADDR_OF_HOST_A
        right=IP_ADDR_OF_HOST_B
        authby=secret
        esp=aes128-sha1
        ike=aes128-sha-modp1024
        type=transport
        compress=no
        auto=add
```
replacing `IP_ADDR_OF_HOST_A` and `IP_ADDR_OF_HOST_B` accordingly.
Left is the local side.

Add this snippet to /etc/ipsec.secrets
```
IP_ADDR_OF_HOST_A IP_ADDR_OF_HOST_B : PSK   "PASSWORD_HERE"
```
again with the replacement from above, and replace `PASSWORD_HERE` with the agreed on password.

Reload config:
```
ipsec reload
```

3. Start & Test connection:
```
ipsec up ipv6-p1-p2
```

4. Debug
- `ipsec status`
- `setkey -D`


### DTLS
As an alternative to IPSec SSL for UDP (DTLS) can be used to authenticate & encrypt.
Example:
```
openssl s_server -dtls -cipher ECDHE-PSK-AES256-CBC-SHA384 -nocert -psk 123456
```
```
openssl s_client -dtls -psk 123456
```

Code here https://github.com/openssl/openssl/blob/master/apps/s_server.c
can be modified to implement this. Should be integrated into the coed in a modular
way: TeleOpEndpointWithTLS
Override relevant methods


Notes:
- https://gist.github.com/Jxck/b211a12423622fe304d2370b1f1d30d5
- https://wiki.openssl.org/index.php/Simple_TLS_Server
- https://mta.openssl.org/pipermail/openssl-users/2018-August/008498.html
- https://www.openssl.org/docs/man1.1.1/man3/SSL_stateless.html

# TODO
Goal of the minimal demo is to verify the practical impact of latency on the teleoperation fidelity and stability. Once this is established, move on to a more fancy setup.

Minmal:
- translation only
- stupid joint impedance nullspace control
- udp bidirectional communication
- statemachine for startup
- timestamping of packages
- time sync via pptp -> LTE?
- stiffness ramping and online tuning
- soft stop
- robot wrapper similar to talos

- statemachines:
    master:
        standby  (hold master with weak joint impedance)
        starting_free (temporary fading from standby to free)
        free (master free, only nullspace control)
        starting_feedback (temporary fading from free to feedback
        feedback (master replicates slave forces)
        stopping (immediate stop of forwarding master state, temporary transition to joint impedance)
    slave:
        standby (hold slave with velocity control)
        starting_slave (temporary fade to accept desired velocity
        slave (running position control, accepting desired velocity)
            high external forces -> standby
            high control error -> standby
        move_back (interpolate back to initial position)
- packet:
    master->slave:
        timestamp
        cart_pos, cart_vel
        master_state
    slave->master:
        timestamp
        cart_wrench
        slave_state
- logging facilities
    master:
        cyclic triggered:
            timestamp
            joint state: q, dq, tau_msr
        communication triggered:
            receive timestamp
            complete packet decoded
    slave:
        cyclic triggered:
            tau_d
            joint state: q, dq
        communication triggered:
            packet serial
            complete packet decoded


TODO:
- [ ] debug network issues
    - [ ] UDP port closed reporting -> upgrade linux kernel
    - [ ] UDP receive buffer overflow
        - [ ] check if ioctl reports high buffer content
        - [ ] check why recv is not keeping up with traffic
        - [ ] check if larger buffer is helpful
    - [ ]  check why disk access slows down inrelated threads -> franka communication exception
- [x] UDP communication
- [x] timestamps
- [/] logging to disk/feed for plotting
- [x] Statemachines
- [x] Controller integration: Panda
- [x] Set up netem to simulation network loss
        https://wiki.linuxfoundation.org/networking/netem#Emulating_wide_area_network_delays
        man netem
- [ ] Shoot next video Panda2Panda
      - [ ] Whiteboard writing
      - [ ] Build 5G House
- [/] Control code
    - [ ] packet order check
    - [ ] better filter on velocity level: luenberger?
        - [ ] deal with time-discrete updates
    - [ ] energy tank stability -> space communication papers
        - [ ] measure energy input my users
        - [ ] measure energy create on slave side
        - [ ] energy on "link"
- [/] fix gstreamer dropouts
    - [x] mtu problem -> fixed mtu=1200
    - [/] find source of lost packets (not on labwork4, not on 802.11ad bridge, no loss when streaming to labwork3 -> inet related)
    - [ ] build gstreamer pipeline which is robust to packet loss using rtpjitterbuffer
        https://gstreamer.freedesktop.org/documentation/rtpmanager/index.html?gi-language=c
    - [x] increase keyframe rate to repair dropouts quicker
- [ ] fix robot exceptions on slave
    - [x] plot input signal to understand problem -> this is network jitter
    - [ ] acceleration limiter filter
    - [x] filter late packets
    - [x] custom controller -> no ikin limits
        -> [ ] fix vibrations/nullspace controller -> damped inverse, nullspace controller
        -> [ ] singularity avoidance controller for master/slave
        -> [ ] endstop potentials on master/slave with vibration key
        -> [ ] lower force feedback in singular configurations -> vibrations
        -> [ ] stiffer table to reduce observer error
- [ ] talos control
    - [ ] code integration
        - [x] look at glove teleop code
        - [x] code integration how? -> reimplement, not inherit
        - [ ] Make abstract CartesianTrajectoryGenerator interface and inherit from this
        - [ ] allow switching between different CartesianTrajecotryGenerator instances at run time (to allow initialization)
        - [ ] Inside TeleoperationCartesianTrajectoryGenerator do everything, query model/robot instance for FTS signal
        - [ ] TODO: how to access gripper? -> "WholeBodyTrajectoryGenerator" problem
                how to control other DOF? head, second arm?
                Have singleton which recieves UDP stream and CartesianTrajectoryGenerator which forwards it to the controller
                Have connection multiplexer in singleton which issues callback 
    - [ ] external force observer? (difficult for floating base)
        - [ ] use just FTS directly or desired wrench of controller
    - [ ] stability problems? -> safety measures -> already in place
    - [ ] testing in same room?
    - [ ] stiff position controller necessary? -> better use feed forward terms
        - [ ] evaluate without force feedback first
        - [ ] calculate acceleration on master side
- [/] integrated link property statistics:
    - [/] latency
    - [ ] jitter
    - [ ] loss
- [x] teleoperation bandwidth: 100KB/s bidirectional -> 0.8Mbit/s
    - [ ] try 500Hz/250Hz sampling
    - [ ] try acceleration change based transmission
- [x] video link bandwidth: 40KB/s -> 0.3Mbit/s (1080p @ 30Hz crazy compression)
- [ ] impulse based force observer: is this the right approach?
    - [ ] table problem
- [x] communicate intermediate results
- [x] desktop edition: spacemouse control with force visualization -> spacemouse_input.cpp
- [ ] Video setup
    - [ ] 2x External CAM
    - [ ] sound
    - [ ] screen capture of live plot
    - [ ] fulfill meaningful task -> story in a picture
        - [ ] bolt tightening: car wheel? -> impulse gun
        - [ ] full on remote surgery: FYDP project link
    - [ ] user on chair
    - [x] user has video link on screen -> bandwidth needed
- [x] fix IPv4
- [x] 6D force feedback
- [x] gripper operation from keyboard input
- [x] indexing from keyboard input
- [x] position/velocity scaling
- [x] ipsec transport mode
        http://www.ipsec-howto.org/x304.html
        http://andersonfam.org/2014/04/02/ipsec-transport-mode/
- [ ] DNS resolution
- [/] public access UDP port to one labwork computer
- [ ] live plotting to show delay
- [ ] network setup for franka2franka setup
        (plug modem to local switch on different subnet?)
- [x] TOS fields for QOS
- [x] minimize packet size -> floats
- [-] stiffer position controller
- [ ] nullspace control of master
- [ ] nullspace control of slave
- [ ] state of the art paper about this?
- [x] connection multiplexing of video stuff
        - [x] send video over the control connection
        - [ ] open gstreamer window directly
            https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html?gi-language=c
        - [ ] prioritize video TOS correctly
            https://stackoverflow.com/questions/14388706/how-do-so-reuseaddr-and-so-reuseport-differ/14388707#14388707
Extensions:
- [ ] ptp sync for evaluation
- [ ] characterize network link with iperf3 (jitter), owamp
- [ ] handle design
- [ ] bimanual control


## Jitter mitigation
- Low pass filter -> instability, low performance
- 


