#include <teleop/teleop.hpp>
#include <iostream>
#include <pbwbc/TeleOpProxy.hpp>
#include <teleop/logger.hpp>

int main(int argc, char **argv) {
    teleop::configure_log4cxx();
    teleop::set_process_priority(10);
    std::string remote_addr;
    if(argc>1) {
        remote_addr = std::string(argv[1]);
        std::cout << "Got slave address: " << remote_addr << std::endl;
    }
    pbwbc::TeleOpProxy proxy(remote_addr,0,250);
    teleop::SpaceMouseMaster master(remote_addr,101,201,103);
    master.execute();

    return 0;
}
