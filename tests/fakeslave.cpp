#include <teleop/teleop.hpp>
#include <pbwbc/TeleOpProxy.hpp>
#include <teleop/logger.hpp>

int main(int argc, char **argv) {
    teleop::configure_log4cxx();
    teleop::set_process_priority(10);
    std::string remote_addr;
    if(argc>1) {
        remote_addr = std::string(argv[1]);
    }
    pbwbc::TeleOpProxy proxy(remote_addr,2324,250);
    teleop::FakeSlave slave(remote_addr,101);
    slave.execute();

    return 0;
}
