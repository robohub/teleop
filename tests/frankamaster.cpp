#include <teleop/teleop_franka.hpp>
#include <pbwbc/TeleOpProxy.hpp>
#include <teleop/logger.hpp>

int main(int argc, char **argv) {
    teleop::configure_log4cxx();
    teleop::set_process_priority(10);
    std::string remote_addr;
    if(argc>1) {
        remote_addr = std::string(argv[1]);
    }
    // forward camera
    // receive over TeleOpEndpoint channel 250,
    // send to [::1]:2325 (hardcoded config)
    pbwbc::TeleOpProxy proxy(remote_addr,0,250);
    teleop::FrankaMaster master(remote_addr,101,201,103);
    master.execute();

    return 0;
}
